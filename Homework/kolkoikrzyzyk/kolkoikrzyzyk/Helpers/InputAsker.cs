﻿using System;
using kolkoikrzyzyk.Models;

namespace kolkoikrzyzyk
{
    public class InputAsker
    {
        public int TakeBoardSize()
        {
            while (true)
            {
                Console.Clear();
                var size = new InputHelper().GetInt("Ile rzędów ma mieć plansza (między 3 a 10)");
                if (size >= 3 && size <= 10)
                {
                    return size;
                }
            }
        }

        public int TakeNumberOfPlayers()
        {
            while (true)
            {
                Console.Clear();
                var numbersOfPlayers = new InputHelper().GetInt("1. Game with AI \n"+"2. Game for Two \n"+"3. Exit");
                if (numbersOfPlayers >= 1 && numbersOfPlayers <= 3)
                {
                    return numbersOfPlayers;
                }
            }
        }
        
        public Gamer TakePlayer1Details()
        {
            Console.Clear();
            var tempPlayer = new Gamer()
                {Name = "Computer"};
            var player1 = new Gamer
            {
                Name = new InputHelper().GetPlayerName("Proszę podać imie pierwszego gracza", tempPlayer.Name),
                plaingSign = new InputAsker().ChoiceSign()
            };
            return player1;
        }

        public Gamer TakePlayer2Details(Gamer player1)
        {
            Console.Clear();
            var player2 = new Gamer {Name = new InputHelper().GetPlayerName("Proszę podać imie drugiego gracza",player1.Name)};
            if (player1.plaingSign == "X")
            {
                player2.plaingSign = "O";
            }
            else
            {
                player2.plaingSign = "X";
            }
            
            return player2;
        }

        public Gamer GiveComputerDetails(string player1Sign)
        {
            var player2 = new Gamer {Name = "Computer"};
            if (player1Sign == "X")
            {
                player2.plaingSign = "O";
            }
            else
            {
                player2.plaingSign = "X";
            }
            
            return player2;
        }

        public string ChoiceSign()
        {
            while (true)
            {
                Console.Clear();
                var playerSign = new InputHelper().GetString("Proszę wybrać 'X' lub 'O'");
                    if (playerSign == "X" || playerSign == "O")
                    {
                        return playerSign;
                    }
            }
        }

        public int TakeVauleForWin(DataHolder dataHolder)
        {
            while (true)
            {
                Console.Clear();
                var ValueForWin =
                    new InputHelper().GetInt("podaj długość ciągu który ma wygrywać (między 3 a " + dataHolder.BoardSize + ")");
                if (ValueForWin >= 3 && ValueForWin <= dataHolder.BoardSize)
                {
                    return ValueForWin;
                }
            }
        }
        
        public string TakeMove()
        {
            return  new InputHelper().GetString("Proszę wpisać wartość pola (przykład: A2)");
        }
    }
}