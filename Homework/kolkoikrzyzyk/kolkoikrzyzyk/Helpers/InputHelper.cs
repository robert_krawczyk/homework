﻿using System;
using kolkoikrzyzyk.Models;

namespace kolkoikrzyzyk
{
    public class InputHelper
    {
        public string GetPlayerName(string message, string Name)
        {
            Console.WriteLine(message);
            var inputtext = "Computer";
            while (inputtext == "Computer" || inputtext == Name)
            {
                inputtext = Console.ReadLine();
                Console.WriteLine("To imię nie może być użyte");
                Console.WriteLine("Proszę podać inne imię");
                
            }
            return inputtext;
        }
        public int GetInt(string message)
        {
            int number = new int();
            Console.WriteLine(message);
            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("To nie jest liczba");
            }
            return number;
        }

        public string GetString(string message)
        {
            Console.WriteLine(message);
            var inputtext = Console.ReadLine();
            return inputtext;
        }
    }
}
