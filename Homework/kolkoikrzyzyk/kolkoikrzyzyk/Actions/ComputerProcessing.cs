﻿using System;
using System.Collections.Generic;
using kolkoikrzyzyk.Models;

namespace kolkoikrzyzyk
{
    public class ComputerProcessing
    {   
        // algorytm sprawdza czy gracz wpisał 2 znaki koło siebie
        // poziomo (-), pionowo (|), lub po skosie (\ /)
        // jesli tak to wstawia swój znak za nimi, a jeśli to nie jest możliwe
        // to przed nimi
        // sprawdza w kolejności poziomo, pionowo, slash (\), backslash (/)
        // jeśli nie uda mu się znaleźć pola do zablokowania,
        // wtedy ustawi znak w wybranym wolnym polu
        // poniżej opisane poszczególne sekwencje i działanie metod

        
        // tworzenie zmiennych potrzebynch w obliczeniach
        private int _fieldIdToUse = 0;
        private int _lastFieldId = 0;
        private int _winingControlValue = 0;
        private int _minFieldContainer = 0;

        // metoda która uruchamia sprawdzanie poszczegolnych kombinacji ułożonych przez gracza (4 przypadki -|\/)
        public void CheckField(DataHolder dataHolder)
        {
            var computerMove = new ComputerProcessing();
            // move zawiera informację czy komputer wykonał już ruch
            var move = 0;
            // sprawdza czy gracz nie ułożył linii poziomo
            move = computerMove.CheckIsSignsAreInLine(1, move, dataHolder);
            // sprawdza czy gracz nie ułożył linii pionowo
            move = computerMove.CheckIsSignsAreInLine(2, move, dataHolder);
            // sprawdza czy gracz nie ułożył linii \
            move = computerMove.CheckIsSignsAreInLine(3, move, dataHolder);
            // sprawdza czy gracz nie ułożył linii /
            move = computerMove.CheckIsSignsAreInLine(4, move, dataHolder);
            // zajmuje własne losowe pole
            new ComputerProcessing().TakeField(move, dataHolder);
        }
        // metoda sprawdzająca czy gracz ułożył przynajmniej dwa znaki koło siebie
        // metoda spradzwa w kolejnosci przypadki dla danego pola i uzywa pierwszy odnaleziony jeśli jest dotępny, jeśli nie to przechodzi dalej 
        private int CheckIsSignsAreInLine(int situation, int move, DataHolder dataHolder)
        {
            var computerMove = new ComputerProcessing();
            if (move == 0)
            {
                var returnData = Tuple.Create<int, int, int>(_winingControlValue, _fieldIdToUse, _lastFieldId);
                _minFieldContainer = new ComputerProcessing().CountMinField(situation , dataHolder);
                foreach (var field in dataHolder.FieldsList)
                {
                    _winingControlValue = 0;
                    _fieldIdToUse = 0;
                    var firstFieldId = dataHolder.FieldsList.IndexOf(field);
                    if (field.Value == dataHolder.Player1.plaingSign)
                    {
                        returnData = FindScore(situation, firstFieldId, _winingControlValue, _fieldIdToUse, dataHolder);
                    }
                    var _minField = firstFieldId - _minFieldContainer;
                    var takenData = computerMove.BlockPlayer(situation, returnData, firstFieldId, _minField, move, dataHolder);
                    move = takenData.Item1;
                    if (move == 1)
                    {
                        break;
                    }
                }
            }
            return move;
        }
        // metoda przekazująca dane o tym które pole ma być zajęte (przed) w przypadku braku możliwiści zajęcia pola za obecnie zajętymi
        private int CountMinField(int situation, DataHolder dataHolder)
        {
            switch (situation)
            {
                case 1:
                    _minFieldContainer = 1;
                    break;
                case 2:
                    _minFieldContainer = dataHolder.BoardSize;
                    break;
                case 3:
                    _minFieldContainer = dataHolder.BoardSize + 1;
                    break;
                case 4:
                    _minFieldContainer = dataHolder.BoardSize - 1;
                    break;
            }
            return _minFieldContainer;
        }
        // metoda zajmująca pole nie będące jeszcze używane
        // zaczyna od środka i następnie dodaje kolejne poziomo
        // wpierw próbuje narastająco nastepnie malejąco

        public DataHolder TakeField(int move, DataHolder dataHolder)
        {
            if (move == 0)
            {
                var i = 0;
                var j = 0;
                var fieldId = new int();
                while (true)
                {
                    var maxValue = (dataHolder.BoardSize * dataHolder.BoardSize) - 1;

                    if (move == 0)
                    {
                        fieldId = maxValue / 2;
                    }
                    else if(move == 0 + i)
                    {
                        fieldId = (maxValue / 2) + i;
                    }
                    else if(move == 0 + j)
                    {
                        fieldId = (maxValue / 2) + j;
                    }
                    move++;
                    i++;
                    j--;
                    //var random = new Random(DateTime.Now.Millisecond);
                    //var fieldId = random.Next(maxValue);
                    foreach (var log in dataHolder.FieldsList)
                    {
                        if (log.Name == "field_" + fieldId && log.Value == " ")
                        {
                            log.Value = dataHolder.Player2.plaingSign;
                            return dataHolder;
                        }
                    }
                }
            }
            return dataHolder;
        }
        //metoda implementowana do metod szukających 2 znaków koło siebie, rozróżnia przypadku dla każdego kierunku.
        public Tuple<int, int, int> FindScore(int situation, int firstFieldId, int winingControlValue,
            int fieldIdToUse, DataHolder dataHolder)
        {
            var returnData = Tuple.Create<int, int, int>(winingControlValue, fieldIdToUse, _lastFieldId);
            switch (situation)
            {
                    
                // przypadek 1 dotyczy linii poziomej
                case 1:
                    _lastFieldId = 0;
                    var boardSize = dataHolder.BoardSize;
                    var fieldsList = dataHolder.FieldsList;
                    for (int i = 0; i < 2; i++)
                    {

                        if (firstFieldId + i >= boardSize * boardSize)
                        {
                            fieldIdToUse = firstFieldId;
                        }
                        var fieldRow = firstFieldId / boardSize;
                        var currentFielsRow = (firstFieldId + i) / boardSize;
                        if (currentFielsRow == fieldRow)
                        {
                            if (fieldsList[firstFieldId].Value != fieldsList[firstFieldId + i].Value)
                            {
                                winingControlValue = 0;
                                break;
                            }
                            _lastFieldId = fieldIdToUse + i;
                            winingControlValue++;
                            fieldIdToUse = firstFieldId + i + 1;
                        }
                    }
                    return returnData = Tuple.Create<int, int, int>(winingControlValue, fieldIdToUse, _lastFieldId);
                // przypadek 2 dotyczy linii pionowej
                case 2:
                    var j = 0;
                    _lastFieldId = 0;
                    boardSize = dataHolder.BoardSize;
                    fieldsList = dataHolder.FieldsList;
                    for (int i = 0; i < 2; i++)
                    {
                        if (firstFieldId + j >= boardSize * boardSize)
                        {
                            winingControlValue = 0;
                            break;
                        }

                        if (fieldsList[firstFieldId].Value != fieldsList[firstFieldId + j].Value)
                        {
                            winingControlValue = 0;
                            break;
                        }
                        _lastFieldId = fieldIdToUse;
                        j = j + boardSize;
                        winingControlValue++;
                        fieldIdToUse = firstFieldId + j;
                    }
                    return returnData = Tuple.Create<int, int, int>(winingControlValue, fieldIdToUse, _lastFieldId);
                // przypadek 3 dotyczy linii \
                case 3:
                    j = 0;
                    var k = 0;
                    _lastFieldId = 0;
                    boardSize = dataHolder.BoardSize;
                    fieldsList = dataHolder.FieldsList;
                    for (int i = 0; i < 2; i++)
                    {
                        if (firstFieldId + j >= boardSize * boardSize)
                        {
                            winingControlValue = 0;
                            break;
                        }

                        var fieldRow = firstFieldId / boardSize;
                        var currentFielsRow = (firstFieldId + j + k) / boardSize;
                        if (currentFielsRow == fieldRow + k)
                        {
                            if (fieldsList[firstFieldId].Value != fieldsList[firstFieldId + j + k].Value)
                            {
                                winingControlValue = 0;
                                break;
                            }
                            _lastFieldId = fieldIdToUse;
                            j = j + boardSize;
                            k++;
                            winingControlValue++;
                            fieldIdToUse = firstFieldId + j + k;
                        }
                    }
                    return returnData = Tuple.Create<int, int, int>(winingControlValue, fieldIdToUse, _lastFieldId);
                    // przypadek 4 dotyczy linii /
                case 4:
                    j = 0;
                    k = 0;
                    _lastFieldId = 0;
                    boardSize = dataHolder.BoardSize;
                    fieldsList = dataHolder.FieldsList;
                    for (int i = 0; i < 2; i++)
                    {
                        if (firstFieldId + j >= boardSize * boardSize)
                        {
                            winingControlValue = 0;
                            break;
                        }

                        var fieldRow = firstFieldId / boardSize;
                        var currentFielsRow = (firstFieldId + j - k) / boardSize;
                        if (currentFielsRow == fieldRow + k)
                        {
                            if (fieldsList[firstFieldId].Value !=
                                fieldsList[firstFieldId + j - k].Value)
                            {
                                winingControlValue = 0;
                                break;
                            }
                            _lastFieldId = fieldIdToUse;
                            j = j + boardSize;
                            k++;
                            winingControlValue++;
                            fieldIdToUse = firstFieldId + j - k;
                        }
                    }
                    return returnData = Tuple.Create<int, int, int>(winingControlValue, fieldIdToUse, _lastFieldId);
            }
            return returnData = Tuple.Create<int, int, int>(winingControlValue, fieldIdToUse, _lastFieldId);
        }
        // metoda wypisujaca znak uniemożliwiajacy stworzenie wiekszej linii niz 2 znaki przez przecwnika, działa wpierw w prawo lub dół
        public Tuple<int, DataHolder> BlockPlayer(int situation, Tuple<int, int, int> takenData, int firstFieldId, int minField,
            int move, DataHolder dataHolder)
        {
            _winingControlValue = takenData.Item1;
            _fieldIdToUse = takenData.Item2;
            _lastFieldId = takenData.Item3;
            var computerMove = new ComputerProcessing();
            if (_winingControlValue == 2)
            {
                var isTrue = computerMove.CountIsThisSameRow(_lastFieldId, _fieldIdToUse, dataHolder);
                if (situation == 1 && isTrue == true || situation != 1 && isTrue == false)
                {
                    if (_fieldIdToUse <dataHolder.BoardSize * dataHolder.BoardSize)
                    {
                        if (dataHolder.FieldsList[_fieldIdToUse].Value == " ")
                        {
                            dataHolder.FieldsList[_fieldIdToUse].Value = dataHolder.Player2.plaingSign;
                            move = 1;
                        }
                        else
                        {
                            move = 0;
                        }
                    }
                    if(move == 0)
                    {
                        if (minField < 0)
                        {
                            move = 0;
                        }
                        else
                        {
                            if (dataHolder.FieldsList[minField].Value == " ")
                            {
                                dataHolder.FieldsList[minField].Value = dataHolder.Player2.plaingSign;
                                move = 1;
                            }
                            else
                            {
                                move = 0;
                            }
                        }
                    }
                }
            }
            var returnData = new Tuple<int, DataHolder>(move, dataHolder);
            return returnData;
        }
        // metoda sprawdzająca czy ostatnie pole ze znakiem przeciwnika oraz kolejny znak którzy komputer chce użyć znajduja sie w tej samej linii (powinno byc true dla 1 oraz false dla 2-4)
        private bool CountIsThisSameRow(int lastFieldId, int fieldIdToUse, DataHolder dataHolder)
        {
            var fieldRow = lastFieldId / dataHolder.BoardSize;
            var currentFielsRow = fieldIdToUse / dataHolder.BoardSize;
            return currentFielsRow == fieldRow;
        }
    }
}

    

