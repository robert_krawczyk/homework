﻿using System;
using System.Collections.Generic;
using System.Reflection;
using kolkoikrzyzyk.Models;

namespace kolkoikrzyzyk
{
    public class BoardActions
    { 
        
        public DataHolder MakeBoard(DataHolder dataHolder)
        {
            var fieldsList = dataHolder.FieldsList;
            var rowsList = dataHolder.RowsList;
            var boardSize = dataHolder.BoardSize;
            int maxSize = boardSize * boardSize;

            for (int i = 0; i < boardSize; i++)
            {
                Row row = new Row()
                {
                    Name = string.Format("{0}", Convert.ToChar('A' + i)),
                    Value = i,

                };
                rowsList.Add(row);
            }
            dataHolder.RowsList = rowsList;
            
            for (int a = 0; a < maxSize; a++)
            {
                var field = new Field
                {
                    Name = "field_" + a,
                    Value = " "
                };

                fieldsList.Add(field);
            }
            dataHolder.FieldsList = fieldsList;
            return dataHolder;
        }
    }
}
