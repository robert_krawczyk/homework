﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using kolkoikrzyzyk.Models;

namespace kolkoikrzyzyk
{
    public class ScoreActions
    {
        
        public void CheckScore(Gamer player, int situation, DataHolder dataHolder)
        {
            var winingControlValue = 0;
            var scoreAnalizer = new ScoreActions();
            foreach (var field in dataHolder.FieldsList)
            {
                var firstFieldId = dataHolder.FieldsList.IndexOf(field);
                if (field.Value == player.plaingSign)
                {
                    winingControlValue = scoreAnalizer.AnalizeScorePosibility(situation, firstFieldId, winingControlValue, dataHolder);

                }
                if (winingControlValue == dataHolder.ValueForWin)
                {
                    new GameActions().WiningInfo(player, dataHolder);
                }
                else
                {
                    winingControlValue = 0;
                }
            }
        }

        public int AnalizeScorePosibility(int situation, int firstFieldId, int winingControlValue, DataHolder dataHolder)
        {
            switch (situation)
            {
                case 1:
                    for (int i = 0; i < dataHolder.ValueForWin; i++)
                    {
                        if (firstFieldId + i >= dataHolder.BoardSize * dataHolder.BoardSize)
                        {
                            winingControlValue = 0;
                            break;
                        }
                        var fieldRow = firstFieldId / dataHolder.BoardSize;
                        var currentFielsRow = (firstFieldId + i) / dataHolder.BoardSize;
                        if (currentFielsRow == fieldRow)
                        {
                            if (dataHolder.FieldsList[firstFieldId].Value != dataHolder.FieldsList[firstFieldId + i].Value)
                            {
                                winingControlValue = 0;
                                break;
                            }
                            winingControlValue++;
                        }
                    }
                    return winingControlValue;
                case 2:
                    var j = 0;
                    for (int i = 0; i < dataHolder.ValueForWin; i++)
                    {
                        if (firstFieldId + j >= dataHolder.BoardSize * dataHolder.BoardSize)
                        {
                            winingControlValue = 0;
                            break;
                        }
                        if (dataHolder.FieldsList[firstFieldId].Value != dataHolder.FieldsList[firstFieldId + j].Value)
                        {
                            winingControlValue = 0;
                            break;
                        }
                        j = j + dataHolder.BoardSize;
                        winingControlValue++;
                    }
                    return winingControlValue;
                case 3:
                    j = 0;
                    var k = 0;
                    for (int i = 0; i < dataHolder.ValueForWin; i++)
                    {
                        if (firstFieldId + j >= dataHolder.BoardSize * dataHolder.BoardSize)
                        {
                            winingControlValue = 0;
                            break;
                        }
                        var fieldRow = firstFieldId / dataHolder.BoardSize;
                        var currentFielsRow = (firstFieldId + j + k) / dataHolder.BoardSize;
                        if (currentFielsRow == fieldRow + k)
                        {
                            if (dataHolder.FieldsList[firstFieldId].Value != dataHolder.FieldsList[firstFieldId + j + k].Value)
                            {
                                winingControlValue = 0;
                                break;
                            }
                            j = j + dataHolder.BoardSize;
                            k++;
                            winingControlValue++;
                        }
                        else
                        {
                            break;

                        }
                    }
                    return winingControlValue;
                case 4:
                    j = 0;
                    k = 0;
                    for (int i = 0; i < dataHolder.ValueForWin; i++)
                    {
                        if (firstFieldId + j >= dataHolder.BoardSize * dataHolder.BoardSize)
                        {
                            winingControlValue = 0;
                            break;
                        }
                        var fieldRow = firstFieldId / dataHolder.BoardSize;
                        var currentFielsRow = (firstFieldId + j - k) / dataHolder.BoardSize;
                        if (currentFielsRow == fieldRow + k)
                        {
                            if (dataHolder.FieldsList[firstFieldId].Value != dataHolder.FieldsList[firstFieldId + j - k].Value)
                            {
                                winingControlValue = 0;
                                break;
                            }
                            j = j + dataHolder.BoardSize;
                            k++;
                            winingControlValue++;
                        }
                        else
                        {
                            break;

                        }
                    }
                    return winingControlValue;
            }
            return winingControlValue;
        }

        public void CheckDraw(DataHolder dataHolder)
        {
            var _fieldList = dataHolder.FieldsList;
            if (_fieldList.Any(x => x.Value == " "))
            {
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Remis");
                Console.WriteLine();
                Console.WriteLine("Naciśnij dowolny klawisz aby rozpocząć od nowa.");
                Console.ReadKey();
                new MainProgram().StartProgram();
            }
        }

    }

}
