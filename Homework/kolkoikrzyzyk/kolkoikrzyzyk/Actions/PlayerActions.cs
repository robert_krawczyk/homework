﻿using kolkoikrzyzyk.Models;

namespace kolkoikrzyzyk
{
    public class PlayerActions
    {
        public DataHolder TakePlayersDetials(DataHolder dataHolder)
        {
            var inputAsker = new InputAsker();
            dataHolder.NumbersOfPlayers = inputAsker.TakeNumberOfPlayers();
            if (dataHolder.NumbersOfPlayers == 1)
            {
                dataHolder.Player1 = inputAsker.TakePlayer1Details();
                dataHolder.Player2 = inputAsker.GiveComputerDetails(dataHolder.Player1.plaingSign);
                return dataHolder;
            }
            else if (dataHolder.NumbersOfPlayers == 2)
            {
                dataHolder.Player1 = inputAsker.TakePlayer1Details();
                dataHolder.Player2 = inputAsker.TakePlayer2Details(dataHolder.Player1);
                return dataHolder;
            }
            return dataHolder;
        }

        public Gamer TakePlayerData(DataHolder dataHolder)
        {
            Gamer player = new Gamer();
            if (dataHolder.CurrentPlayer == 1)
            {
                 player =  dataHolder.Player1;
            }
            else if (dataHolder.CurrentPlayer == 2)
            {
                 player = dataHolder.Player2;
            }
            return player;
        }

       
    }
}
