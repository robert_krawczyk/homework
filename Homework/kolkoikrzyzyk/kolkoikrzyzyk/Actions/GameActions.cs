﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using kolkoikrzyzyk.Models;

namespace kolkoikrzyzyk
{
    public class GameActions
    {
        public DataHolder BuildGame(DataHolder dataHolder)
        {
            var inputAsker = new InputAsker();
            dataHolder.FieldsList = new List<Field>();
            dataHolder.RowsList = new List<Row>();
            dataHolder.WiningControlValue = 0;
            dataHolder.CurrentPlayer = 1;
            dataHolder.BoardSize = inputAsker.TakeBoardSize();
            dataHolder.ValueForWin = inputAsker.TakeVauleForWin(dataHolder);
            dataHolder = new BoardActions().MakeBoard(dataHolder);
            return dataHolder;
        }
    
        public void RefreshBoard(DataHolder dataHolder)
        {
            var boardSize = dataHolder.BoardSize;
            var fieldsList = dataHolder.FieldsList;
            var rowsList = dataHolder.RowsList;

            Console.Clear();
            var c = 0;
            Console.Write("  ");

            for (int i = 0; i < boardSize; i++)
            {
                Console.Write(rowsList[i].Name + " ");
            }
            Console.WriteLine();
            Console.Write(" |");
            for (int i = 0; i < boardSize; i++)
            {
                Console.Write("-|");
            }
            Console.WriteLine();
            for (int j = 0; j < boardSize; j++)
            {

                Console.Write(j);
                for (int k = 0; k < boardSize; k++)
                {


                    Console.Write("|");

                    foreach (var log in fieldsList)
                    {
                        if (log.Name == "field_" + c)
                        {
                            Console.Write(log.Value);
                        }
                    }
                    c++;
                }
                Console.WriteLine("|");

                Console.Write(" |");
                for (int i = 0; i < boardSize; i++)
                {
                    Console.Write("-|");
                }
                Console.WriteLine();
            }

        }

        public DataHolder CheckFields(DataHolder dataHolder)
        {
            var inputAsker = new InputAsker();
            var game = new GameActions();
            var player = new PlayerActions().TakePlayerData(dataHolder);
            if (player.Name != "Computer")
            {
                Console.WriteLine(player.Name + " proszę wybrać wolne pole:");
                var move = inputAsker.TakeMove();
                int tempColumn = 0;
                int tempRow = 0;
                var correctLetter = dataHolder.RowsList.Exists(x => x.Name == move[0].ToString());
                foreach (var letter in dataHolder.RowsList)
                {
                        if (letter.Name == move[0].ToString())
                        {
                            tempColumn = letter.Value;
                            tempRow = (int) Char.GetNumericValue(move[1]);
                        }
                }
                
                int fieldId = (tempRow * dataHolder.BoardSize) + tempColumn;
                foreach (var log in dataHolder.FieldsList)
                {
                    if (correctLetter == true && (int)Char.GetNumericValue(move[1]) <= dataHolder.BoardSize - 1)
                    {
                        if (log.Name == "field_" + fieldId)
                        {
                            if (log.Value != " ")
                            {
                                game.RefreshBoard(dataHolder);
                                Console.WriteLine("To pole jest już zajęte");
                                game.CheckFields(dataHolder);
                                break;
                            }
                            else
                            {
                                log.Value = player.plaingSign;
                            }
                        }
                    }
                    else
                    {
                        game.RefreshBoard(dataHolder);
                        var temp = dataHolder.RowsList.Count;
                        var temp2 = dataHolder.RowsList[temp - 1].Name;
                        Console.WriteLine("Wartość z poza skali. Wybierz między A i " + temp2 + " oraz między 0 a " + (dataHolder.BoardSize - 1));
                        game.CheckFields(dataHolder);
                        break;
                    }
                }
            }
            else
            {
                new ComputerProcessing().CheckField(dataHolder);
            }
            return dataHolder;
        }

        public void CheckIsWin(DataHolder dataHolder)
        {
            var scoreAnalizer = new ScoreActions();
            var player = new PlayerActions().TakePlayerData(dataHolder);
            for (int i = 1; i <= 4; i++)
            {
                scoreAnalizer.CheckScore(player, i, dataHolder);
            }
            scoreAnalizer.CheckDraw(dataHolder);
        }

        public void ChangePlayer(DataHolder dataHolder)
        {
            if (dataHolder.CurrentPlayer == 1)
            {
                var currentPlayer = 2;
                dataHolder.CurrentPlayer = currentPlayer;
            }
            else 
            {
                var currentPlayer = 1;
                dataHolder.CurrentPlayer = currentPlayer;
            }
        }

        public void WiningInfo(Gamer player, DataHolder dataHolder)
        {

            Console.Clear();
            new GameActions().RefreshBoard(dataHolder);
            if (player.Name != "Computer")
            {
                Console.WriteLine(" Gratulacje, " + player.Name + " wygrałeś, grając '" + player.plaingSign + "'!");
            }
            else
            {
                Console.WriteLine("Niestety, ale wygrał " + player.Name + " który grał '" +
                                  player.plaingSign + "'!");
            }
            Console.WriteLine();
            Console.WriteLine("Naciśnij dowolny klawisz aby rozpocząć ponownie.");
            Console.ReadKey();
            new MainProgram().StartProgram();
        }
    }
}
