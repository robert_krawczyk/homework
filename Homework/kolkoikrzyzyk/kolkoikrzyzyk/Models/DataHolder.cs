﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace kolkoikrzyzyk.Models
{
    public class DataHolder
    {
        public List<Field> FieldsList;
        public List<Row> RowsList;
        public int WiningControlValue;
        public int CurrentPlayer;
        public int BoardSize;
        public int ValueForWin;
        public Gamer Player1;
        public Gamer Player2;
        public int NumbersOfPlayers;
    }
}
