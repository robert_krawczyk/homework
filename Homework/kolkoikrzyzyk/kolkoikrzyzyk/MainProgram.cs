﻿using System;
using System.Collections.Generic;
using System.Linq;
using kolkoikrzyzyk.Models;

namespace kolkoikrzyzyk
{
    internal class MainProgram
    {
        public void StartProgram()
        {
            var game = new GameActions();
            var dataHolder = new DataHolder();
            dataHolder = new PlayerActions().TakePlayersDetials(dataHolder);
            if (dataHolder.NumbersOfPlayers != 3)
            {
                dataHolder = game.BuildGame(dataHolder);
                while (true)
                {
                    game.RefreshBoard(dataHolder);
                    game.CheckFields(dataHolder);
                    game.CheckIsWin(dataHolder);
                    game.ChangePlayer(dataHolder);
                }
            }
            else
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
        }
    }
}