﻿using System.Text;
using BusisnessLayer.ModelsDto;

namespace TreningHomework.EventsAndKernels
{
    public class RaportTakeEventArgs
    {
        public RaportDto raportText;
    }
}