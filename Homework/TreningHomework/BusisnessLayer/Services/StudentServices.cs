﻿using System.Collections.Generic;
using System.Linq;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using DataLayer.Models;
using DataLayer.Repositories;


namespace BusisnessLayer.Services
{
    public class StudentServices : IStudentServices
    {
        private GenericRepositoryUnitOfWork _repository;
        private IDtoToEntityMappers _dtoToEntity;
        private IEntityToDtoMappers _entityToDto;
        public StudentServices(IDtoToEntityMappers dtoToEntity, IEntityToDtoMappers entityToDtoMappers)
        {
            _dtoToEntity = dtoToEntity;
            _entityToDto = entityToDtoMappers;
            _repository = GenericRepositoryUnitOfWork.GetInstance();

        }
        
        public bool StudentAdd(StudentDto studentDto)
        {
            var student = _dtoToEntity.StudentDtoToEntity(studentDto);
            return _repository.GetRepository<Student>().Add(student);
            _repository.SaveChanges();
        }

        public List<StudentDto> GetStudentsList()
        {
            var studentList = _repository.GetRepository<Student>().GetAll().ToList();
            var studentListDto = _entityToDto.StudentListEntityToDto(studentList);
            return studentListDto;
        }

        public bool UpdateStudent(StudentDto studentDto)
        {
            var student = _dtoToEntity.StudentDtoToEntity(studentDto);
            return _repository.GetRepository<Student>().Update(student);
            _repository.SaveChanges();
        }

        public StudentDto GetStudentById(int studentId)
        {
            var student = _repository.GetRepository<Student>().GetById(studentId);
            return _entityToDto.StudentEntityToDto(student);
        }
    }
}
