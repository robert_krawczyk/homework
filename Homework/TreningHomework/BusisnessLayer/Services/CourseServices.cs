﻿using System.Collections.Generic;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using DataLayer.Models;
using DataLayer.Repositories;
using DataLayer.Services;

namespace BusisnessLayer.Services
{
    public class CourseServices : ICourseServices
    {
        private IDtoToEntityMappers _dtoToEntity;
        private IEntityToDtoMappers _entityToDto;
        private GenericRepositoryUnitOfWork _repository;
        private GenericRepositoryServices<Course> _repositoryServices;
        public CourseServices(IDtoToEntityMappers dtoToEntity, IEntityToDtoMappers entityToDtoMappers)
        {
            _dtoToEntity = dtoToEntity;
            _entityToDto = entityToDtoMappers;
            _repositoryServices = new GenericRepositoryServices<Course>();
            _repository = GenericRepositoryUnitOfWork.GetInstance();
            
        }
        
        public bool CourseAdd(CourseDto courseDto)
        {
            var course = _dtoToEntity.CourseDtoToEntity(courseDto);
            return _repository.GetRepository<Course>().Add(course);
            _repository.SaveChanges();
        }

        public List<CourseDto> GetCoursesList()
        {
            var coursesList = _repositoryServices.GetCourseListIncludedStudents();
            return _entityToDto.CoursesListEntityToDto(coursesList);
        }

        public CourseDto GetCourseById(int Id)
        {
            var course = _repositoryServices.GetCourseByIdIncludedAllLists(Id);
            return _entityToDto.CourseEntityToDto(course);
        }

        public bool UpdateCourse(CourseDto courseDto)
        {
            var course = _dtoToEntity.CourseDtoToEntity(courseDto);
            return _repository.GetRepository<Course>().Update(course);
            _repository.SaveChanges();
        }
    }
}
