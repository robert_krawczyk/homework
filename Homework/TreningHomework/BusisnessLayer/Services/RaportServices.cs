﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using DataLayer.Models;
using DataLayer.Services;
using TreningHomework.EventsAndKernels;



namespace BusisnessLayer.Services
{
    public class RaportServices : IRaportServices
    {
        private RaportDto _raport;
        private CourseDto _courseDto;
        public delegate void TakeRaportEventHandler(object sender, RaportTakeEventArgs args);
        public event TakeRaportEventHandler TakeRaport;
        public PresentDto studentPresentDetails = new PresentDto();
        public HomeworkDto studentHwDetails = new HomeworkDto();
        private IEntityToDtoMappers _entityToDto;
        private GenericRepositoryServices<Course> _genericRepository;
        public RaportServices(IEntityToDtoMappers entityToDtoMappers)
        {
            _entityToDto = entityToDtoMappers;
            _genericRepository = new GenericRepositoryServices<Course>();
        }

        public void TakeDataToRaport(int courseId)
        {
            var course = _genericRepository.GetCourseByIdIncludedAllLists(courseId);
            _courseDto = _entityToDto.CourseEntityToDto(course);
            
             _raport = new RaportDto();
            _raport.course = _courseDto;
            _raport.presents = new List<PresentDto>();
            _raport.homeworks = new List<HomeworkDto>();
            var bgWork = new BackgroundWorker();
            bgWork.DoWork += CheckPresentList;
            bgWork.DoWork += CheckHomeworkList;
            bgWork.RunWorkerAsync();
            

            
            var raportText = _raport;
            OnRaportTaken(raportText);
        }

       


        private void OnRaportTaken(RaportDto raportText)
        {
            if (TakeRaport == null)
            {
                return;
            }

            var eventArgs = new RaportTakeEventArgs();
            eventArgs.raportText = raportText;

            TakeRaport(this, eventArgs);
        }

        private void CheckPresentList(object sender, DoWorkEventArgs e)
        {
            foreach (var student in _courseDto.StudentList)
            {

                if (_courseDto.PresentList.Count != 0)
                {
                    double _isPresentValue = 0;
                    double _shouldBePresentValue = 0;
                    foreach (var present in _courseDto.PresentList)
                    {
                        if (student.Id == present.Student.Id)
                        {
                            _isPresentValue = _isPresentValue + present.IsPresent;
                            _shouldBePresentValue++;
                        }
                    }
                    if (_shouldBePresentValue != 0)
                    {
                        double ProcentOfPresent = 0;
                        ProcentOfPresent = _isPresentValue / _shouldBePresentValue * 100;
                        string pass = "Zaliczone";
                        if (ProcentOfPresent < _courseDto.MinPresent)
                        {
                            pass = "Niezaliczone";
                        }

                        studentPresentDetails.Student = student;
                        studentPresentDetails.ProcentOfPresent = ProcentOfPresent;
                        studentPresentDetails.IsPresentValue = _isPresentValue;
                        studentPresentDetails.ShouldBePresentValue = _shouldBePresentValue;
                        studentPresentDetails.pass = pass;
                        _raport.presents.Add(studentPresentDetails);
                    }
                }
            }
        }

        private void CheckHomeworkList(object sender, DoWorkEventArgs e)
            {
            foreach (var student in _courseDto.StudentList)
            {
                if (_courseDto.HomeworkList.Count != 0)
                {
                    double _hwValue = 0;
                    double _MaxHwValue = 0;
                    foreach (var log in _courseDto.HomeworkList)
                    {
                        if (student.Id == log.Student.Id)
                        {
                            _hwValue = _hwValue + log.Points;
                            _MaxHwValue = _MaxHwValue + log.MaxPoints;
                        }
                    }
                    if (_MaxHwValue != 0)
                    {
                        double ProcentOfHwork = 0;
                        ProcentOfHwork = _hwValue / _MaxHwValue * 100;
                        string pass = "Zaliczone";
                        if (ProcentOfHwork < _courseDto.MinHomework)
                        {
                            pass = "Niezaliczone";
                        }

                        studentHwDetails.Student = student;
                        studentHwDetails.pass = pass;
                        studentHwDetails.Points = _hwValue;
                        studentHwDetails.MaxPoints = _MaxHwValue;
                        studentHwDetails.ProcentOfHomework = ProcentOfHwork;
                        _raport.homeworks.Add(studentHwDetails);
                    }
                }

            }
        }
    }
}
