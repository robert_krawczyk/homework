﻿using System.Collections.Generic;
using BusisnessLayer.ModelsDto;
using DataLayer.Models;

namespace BusisnessLayer.Interfaces
{
    public interface IDtoToEntityMappers
    {
        Course CourseDtoToEntity(CourseDto courseDto);
        Homework HomeworkDtoToEntity(HomeworkDto homeworkDto);
        List<Homework> HomeworkListDtoToEntity(List<HomeworkDto> homeworkDtoList);
        Present PresentDtoToEntity(PresentDto presentDto);
        List<Present> PresentListDtoToEntity(List<PresentDto> presentDtoList);
        Student StudentDtoToEntity(StudentDto studentDto);
        List<Student> StudentListDtoToEntity(List<StudentDto> studentDtoList);
    }
}