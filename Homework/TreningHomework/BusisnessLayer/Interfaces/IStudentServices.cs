﻿using System.Collections.Generic;
using BusisnessLayer.ModelsDto;

namespace BusisnessLayer.Interfaces
{
    public interface IStudentServices
    {
        StudentDto GetStudentById(int studentId);
        List<StudentDto> GetStudentsList();
        bool StudentAdd(StudentDto studentDto);
        bool UpdateStudent(StudentDto studentDto);
    }
}