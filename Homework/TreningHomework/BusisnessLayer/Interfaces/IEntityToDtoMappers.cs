﻿using System.Collections.Generic;
using BusisnessLayer.ModelsDto;
using DataLayer.Models;

namespace BusisnessLayer.Interfaces
{
    public interface IEntityToDtoMappers
    {
        CourseDto CourseEntityToDto(Course course);
        List<int> CourseIdListEntityToDto(List<int> courseId);
        List<CourseDto> CoursesListEntityToDto(List<Course> courses);
        HomeworkDto HomeworkEntityToDto(Homework homework);
        List<HomeworkDto> HomeworkListEntityToDto(List<Homework> homeworksList);
        PresentDto PresentEntityToDto(Present present);
        List<PresentDto> PresentListEntityToDto(List<Present> presentsList);
        StudentDto StudentEntityToDto(Student student);
        List<StudentDto> StudentListEntityToDto(List<Student> studentsList);
    }
}