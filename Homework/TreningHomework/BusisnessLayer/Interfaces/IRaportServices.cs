﻿using BusisnessLayer.Services;
using TreningHomework.EventsAndKernels;

namespace BusisnessLayer.Interfaces
{
    public interface IRaportServices
    {
        event RaportServices.TakeRaportEventHandler TakeRaport;
        void TakeDataToRaport(int courseId);
    }
}