﻿using System.Collections.Generic;
using BusisnessLayer.ModelsDto;

namespace BusisnessLayer.Interfaces
{
    public interface ICourseServices
    {
        bool CourseAdd(CourseDto courseDto);
        CourseDto GetCourseById(int Id);
        List<CourseDto> GetCoursesList();
        bool UpdateCourse(CourseDto courseDto);
    }
}