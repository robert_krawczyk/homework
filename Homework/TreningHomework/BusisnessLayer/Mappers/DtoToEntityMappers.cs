﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using DataLayer.Models;

namespace BusisnessLayer.Mappers
{
    public class DtoToEntityMappers : IDtoToEntityMappers
    {
        public Course CourseDtoToEntity(CourseDto courseDto)
        {
            var course = new Course();
            course.Id = courseDto.Id;
            course.Name = courseDto.Name;
            course.Leader = courseDto.Leader;
            course.StartDate = courseDto.StartDate;
            course.MinHomework = courseDto.MinHomework;
            course.MinPresent = courseDto.MinPresent;
            course.MaxStudents = courseDto.MaxStudents;
            course.PresentList = PresentListDtoToEntity(courseDto.PresentList);
            course.HomeworkList = HomeworkListDtoToEntity(courseDto.HomeworkList);
            course.StudentList = StudentListDtoToEntity(courseDto.StudentList);

            return course;
        }

        public Student StudentDtoToEntity(StudentDto studentDto)
        {
            var student = new Student();
            student.Id = studentDto.Id;
            student.Name = studentDto.Name;
            student.Surname = studentDto.Surname;
            student.BirthDate = studentDto.BirthDate;
            student.Pesel = studentDto.Pesel;
            student.Sex = studentDto.Sex;
            
            return student;
        }

        public Present PresentDtoToEntity(PresentDto presentDto)
        {
            var present = new Present();
            present.Id = presentDto.Id;
            present.Date = presentDto.Date;
            present.IsPresent = presentDto.IsPresent;
            present.Student = StudentDtoToEntity(presentDto.Student);

            return present;
        }

        public Homework HomeworkDtoToEntity(HomeworkDto homeworkDto)
        {
            var homework = new Homework();
            homework.Id = homeworkDto.Id;
            homework.MaxPoints = homeworkDto.MaxPoints;
            homework.Points = homeworkDto.Points;
            homework.Student = StudentDtoToEntity(homeworkDto.Student);

            return homework;
        }

        public List<Student> StudentListDtoToEntity(List<StudentDto> studentDtoList)
        {
            var studentList = new List<Student>();

            foreach (var log in studentDtoList)
            {
                studentList.Add(StudentDtoToEntity(log));
            }

            return studentList;
        }

        public List<Present> PresentListDtoToEntity(List<PresentDto> presentDtoList)
        {
            var presentList = new List<Present>();

            foreach (var log in presentDtoList)
            {
                presentList.Add(PresentDtoToEntity(log));
            }

            return presentList;
        }

        public List<Homework> HomeworkListDtoToEntity(List<HomeworkDto> homeworkDtoList)
        {
            var homeworkList = new List<Homework>();

            foreach (var log in homeworkDtoList)
            {
                homeworkList.Add(HomeworkDtoToEntity(log));
            }
            return homeworkList;
        }
    }
}

