﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using DataLayer.Models;

namespace BusisnessLayer.Mappers
{
    public class EntityToDtoMappers : IEntityToDtoMappers
    {
        public StudentDto StudentEntityToDto(Student student)
        {
            var studentDto = new StudentDto();
            studentDto.Id = student.Id;
            studentDto.Pesel = student.Pesel;
            studentDto.BirthDate = student.BirthDate;
            studentDto.Name = student.Name;
            studentDto.Surname = student.Surname;
            studentDto.Sex = student.Sex;

            return studentDto;
        }

        public PresentDto PresentEntityToDto(Present present)
        {
            var presentDto = new PresentDto();
            presentDto.Id = present.Id;
            presentDto.Student = StudentEntityToDto(present.Student);
            presentDto.Date = present.Date;
            presentDto.IsPresent = present.IsPresent;

            return presentDto;
        }

        public HomeworkDto HomeworkEntityToDto(Homework homework)
        {
            var homworkDto = new HomeworkDto();
            homworkDto.Id = homework.Id;
            homworkDto.Student = StudentEntityToDto(homework.Student);
            homworkDto.MaxPoints = homework.MaxPoints;
            homworkDto.Points = homework.Points;

            return homworkDto;
        }

        public CourseDto CourseEntityToDto(Course course)
        {
            var courseDto = new CourseDto();
            courseDto.Id = course.Id;
            courseDto.Name = course.Name;
            courseDto.Leader = course.Leader;
            courseDto.MinHomework = course.MinHomework;
            courseDto.MinPresent = course.MinPresent;
            courseDto.MaxStudents = course.MaxStudents;
            courseDto.StartDate = course.StartDate;
            courseDto.PresentList = PresentListEntityToDto(course.PresentList);
            courseDto.HomeworkList = HomeworkListEntityToDto(course.HomeworkList);
            courseDto.StudentList = StudentListEntityToDto(course.StudentList);

            return courseDto;
        }

        public List<CourseDto> CoursesListEntityToDto(List<Course> courses)
        {
            var coursesListDto = new List<CourseDto>();

            foreach (var log in courses)
            {
                coursesListDto.Add(CourseEntityToDto(log));
            }
            return coursesListDto;
        }

        public List<PresentDto> PresentListEntityToDto(List<Present> presentsList)
        {
            var presentsListDto = new List<PresentDto>();
            if (presentsList != null)
            {
                foreach (var log in presentsList)
                {
                    presentsListDto.Add(PresentEntityToDto(log));
                }
            }
            return presentsListDto;
        }

        public List<HomeworkDto> HomeworkListEntityToDto(List<Homework> homeworksList)
        {
            var homeworksListDto = new List<HomeworkDto>();
            if (homeworksList != null)
            {
                foreach (var log in homeworksList)
                {
                    homeworksListDto.Add(HomeworkEntityToDto(log));
                }
            }
            return homeworksListDto;
        }

        public List<StudentDto> StudentListEntityToDto(List<Student> studentsList)
        {
            var studentsListDto = new List<StudentDto>();
            if (studentsList != null)
            {
                foreach (var log in studentsList)
                {
                    studentsListDto.Add(StudentEntityToDto(log));
                }
            }
            return studentsListDto;
        }

        public List<int> CourseIdListEntityToDto(List<int> courseId)
        {
            var courseIdDto = new List<int>();
            if (courseId != null)
            {
                foreach (var log in courseId)
                {
                    courseIdDto.Add(log);
                }
            }
            return courseIdDto;
        }
    }
}
