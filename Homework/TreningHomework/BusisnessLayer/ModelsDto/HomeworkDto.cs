﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusisnessLayer.ModelsDto
{
    public class HomeworkDto
    {
        public int Id;
        public StudentDto Student;
        public double MaxPoints;
        public double Points;
        public double ProcentOfHomework;
        public string pass;
    }
}
