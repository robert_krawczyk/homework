﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusisnessLayer.ModelsDto
{
    public class RaportDto
    {
        public CourseDto course;
        public List<StudentDto> students;
        public List<HomeworkDto> homeworks;
        public List<PresentDto> presents;
    }
}
