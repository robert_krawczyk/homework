﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusisnessLayer.ModelsDto
{
    public class PresentDto
    {
        public int Id;
        public StudentDto Student;
        public DateTime Date;
        public double IsPresent;
        public double ProcentOfPresent;
        public double IsPresentValue;
        public double ShouldBePresentValue;
        public string pass;
    }
}
