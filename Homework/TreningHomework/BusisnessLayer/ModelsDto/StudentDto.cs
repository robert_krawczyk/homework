﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusisnessLayer.ModelsDto
{
    public class StudentDto
    {
        public int Id;
        public string Name;
        public string Surname;
        public long Pesel;
        public DateTime BirthDate;
        public int Sex;
    }
}
