﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusisnessLayer.ModelsDto
{
    public class CourseDto
    {
        public int Id;
        public string Name;
        public string Leader;
        public DateTime StartDate;
        public double MinHomework;
        public double MinPresent;
        public int MaxStudents;
        public List<StudentDto> StudentList;
        public List<PresentDto> PresentList;
        public List<HomeworkDto> HomeworkList;
    }
}
