﻿using System;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using BusisnessLayer.Services;
using DataLayer.Models;
using DataLayer.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BusinessLayer.Tests
{
    [TestClass]
    public class StudentServicesTests
    {
        private IDtoToEntityMappers _dtoToEntity;
        private IEntityToDtoMappers _entityToDto;
        
        public StudentServicesTests(IDtoToEntityMappers dtoToEntity, IEntityToDtoMappers entityToDtoMappers)
        {
            _dtoToEntity = dtoToEntity;
            _entityToDto = entityToDtoMappers;
        }

        [TestMethod]
        public void CheckStudentAdd_GiveAllStudentData_TakeTrue()
        {

            var student = new StudentDto();
            student.Name = "Jacek";
            student.Surname = "Kowalski";
            student.BirthDate= DateTime.Today;
            student.Id = 1;
            student.Pesel = 123;
            student.Sex = 2;

            var student2 = new Student();
            student2.Name = "Jacek";
            student2.Surname = "Kowalski";
            student2.BirthDate = DateTime.Today;
            student2.Id = 1;
            student2.Pesel = 123;
            student2.Sex = 2;

            var studentRepositoryMock = new Mock<GenericRepositoryUnitOfWork>();
            studentRepositoryMock.Setup(x => x.GetRepository<Student>().Add(student2)).Returns(true);
            var studentServices = new StudentServices(_dtoToEntity, _entityToDto);
            
            var result = studentServices.StudentAdd(student);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CheckGetStudentById_GiveAllStudentData_TakeStudent()
        {
            var student = new Student();
            student.Name = "Jacek";
            student.Surname = "Kowalski";
            student.BirthDate = DateTime.Today;
            student.Id = 1;
            student.Pesel = 123;
            student.Sex = 2;

            var student2 = new StudentDto();
            student2.Name = "Jacek";
            student2.Surname = "Kowalski";
            student2.BirthDate = DateTime.Today;
            student2.Id = 1;
            student2.Pesel = 123;
            student2.Sex = 2;

            var StudentRepositoryMock= new Mock<GenericRepositoryUnitOfWork>();
            StudentRepositoryMock.Setup(x => x.GetRepository<Student>().GetById(student.Id)).Returns(student);
            var studentServices = new StudentServices(_dtoToEntity, _entityToDto);
            var result = studentServices.GetStudentById(student.Id);

            Assert.AreEqual(result.Name,student2.Name);
            Assert.AreEqual(result.Surname,student2.Surname);
        }
    }
}
