﻿using System;
using System.Text;
using System.Collections.Generic;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using BusisnessLayer.Services;
using DataLayer.Models;
using DataLayer.Repositories;
using DataLayer.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BusinessLayer.Tests
{
   
    [TestClass]
    public class CourseServicesTests
    {
        private IDtoToEntityMappers _dtoToEntity;
        private IEntityToDtoMappers _entityToDto;
        
        public CourseServicesTests(IDtoToEntityMappers dtoToEntity, IEntityToDtoMappers entityToDtoMappers)
        {
            _dtoToEntity = dtoToEntity;
            _entityToDto = entityToDtoMappers;
        }

        [TestMethod]
        public void CheckCourseAdd_GiveCourseData_TakeTrue()
        {
            var courseDto = new CourseDto();
            courseDto.Id = 1;
            courseDto.Name = "Test Raportu";
            courseDto.Leader = "Ja";
            courseDto.StartDate = DateTime.Today;
            courseDto.MinHomework = 80;
            courseDto.MinPresent = 80;
            courseDto.MaxStudents = 12;
            courseDto.StudentList = new List<StudentDto>();
            var studentDto = new StudentDto();
            studentDto.Id = 1;
            studentDto.Name = "John";
            studentDto.Surname = "Nowak";
            studentDto.Pesel = 123;
            studentDto.BirthDate = DateTime.Parse("12.12.2000");
            studentDto.Sex = 2;
            courseDto.StudentList.Add(studentDto);
            var dateDto = new PresentDto();
            dateDto.Id = 1;
            dateDto.Date = DateTime.Today;
            dateDto.IsPresent = 1;
            dateDto.Student = studentDto;
            var dateDtoList = new List<PresentDto>();
            dateDtoList.Add(dateDto);
            courseDto.PresentList = dateDtoList;
            var hworkDto = new HomeworkDto();
            hworkDto.Id = 1;
            hworkDto.MaxPoints = 100;
            hworkDto.Points = 90;
            hworkDto.Student = studentDto;
            var hworkDtoList = new List<HomeworkDto>();
            hworkDtoList.Add(hworkDto);
            courseDto.HomeworkList = hworkDtoList;

            var course = new Course();
            course.Id = 1;
            course.Name = "Test Raportu";
            course.Leader = "Ja";
            course.StartDate = DateTime.Today;
            course.MinHomework = 80;
            course.MinPresent = 80;
            course.MaxStudents = 12;
            course.StudentList = new List<Student>();
            var student = new Student();
            student.Id = 1;
            student.Name = "John";
            student.Surname = "Nowak";
            student.Pesel = 123;
            student.BirthDate = DateTime.Parse("12.12.2000");
            student.Sex = 2;
            course.StudentList.Add(student);
            var date = new Present();
            date.Id = 1;
            date.Date = DateTime.Today;
            date.IsPresent = 1;
            date.Student = student;
            var dateList = new List<Present>();
            dateList.Add(date);
            course.PresentList = dateList;
            var hwork = new Homework();
            hwork.Id = 1;
            hwork.MaxPoints = 100;
            hwork.Points = 90;
            hwork.Student = student;
            var hworkList = new List<Homework>();
            hworkList.Add(hwork);
            course.HomeworkList = hworkList;

            
        var courseRepositoryMock = new Mock<GenericRepositoryUnitOfWork>();
            courseRepositoryMock.Setup(x => x.GetRepository<Course>().Add(course)).Returns(true);
            var courseSevices = new CourseServices(_dtoToEntity, _entityToDto);
            var result = courseSevices.CourseAdd(courseDto);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void CheckGetCoursesList_GiveCourseDateils_TakeList()
        {
            var course = new CourseDto();
            course.Name = "testCourse";
            course.Leader = "Jan Nowak";
            course.StartDate = DateTime.Today;
            course.MinPresent = 80;
            course.MinHomework = 80;
            course.MaxStudents = 12;
            course.Id = 1;
            course.StudentList = new List<StudentDto>();
            course.HomeworkList = new List<HomeworkDto>();
            course.PresentList = new List<PresentDto>();

            var course2 = new Course();
            course2.Name = "testCourse";
            course2.Leader = "Jan Nowak";
            course2.StartDate = DateTime.Today;
            course2.MinPresent = 80;
            course2.MinHomework = 80;
            course2.MaxStudents = 12;
            course2.Id = 1;
            course2.StudentList = new List<Student>();
            course2.HomeworkList = new List<Homework>();
            course2.PresentList = new List<Present>();
            
            var courseRepositoryMock = new Mock<GenericRepositoryUnitOfWork>();
            courseRepositoryMock.Setup(x => x.GetRepository<Course>().GetById(course2.Id)).Returns(course2);
            var courseServices = new CourseServices(_dtoToEntity, _entityToDto);
            var result = courseServices.GetCourseById(course2.Id);

            Assert.AreEqual(result.Id,course.Id);
            Assert.AreEqual(result.Name,course.Name);
        }
    }
}
