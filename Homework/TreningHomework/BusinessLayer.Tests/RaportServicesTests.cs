﻿//using System;
//using System.Collections.Generic;
//using System.Text;
//using BusisnessLayer.ModelsDto;
//using BusisnessLayer.Services;
//using DataLayer.Interfaces;
//using DataLayer.Models;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;

//namespace BusinessLayer.Tests
//{
//    [TestClass]
//    public class RaportServicesTests
//    {
//        [TestMethod]
//        public void CheckTakeDataToRaport_GiveAllData_AreEquil()
//        {
//            var courseDto = new CourseDto();
//            courseDto.Id = 1;
//            courseDto.Name = "Test Raportu";
//            courseDto.Leader = "Ja";
//            courseDto.StartDate = DateTime.Today;
//            courseDto.MinHomework = 80;
//            courseDto.MinPresent = 80;
//            courseDto.MaxStudents = 12;
//            courseDto.StudentList = new List<StudentDto>();
//            var studentDto = new StudentDto();
//            studentDto.Id = 1;
//            studentDto.Name = "John";
//            studentDto.Surname = "Nowak";
//            studentDto.Pesel = 123;
//            studentDto.BirthDate = DateTime.Parse("12.12.2000");
//            studentDto.Sex = 2;
//            courseDto.StudentList.Add(studentDto);
//            var studentDto2 = new StudentDto();
//            studentDto2.Name = "Piotr";
//            studentDto2.Surname = "Mazur";
//            courseDto.StudentList.Add(studentDto2);
//            var dateDto = new PresentDto();
//            dateDto.Id = 1;
//            dateDto.Date = DateTime.Today;
//            dateDto.IsPresent = 1;
//            dateDto.Student = studentDto;
//            var dateDtoList = new List<PresentDto>();
//            dateDtoList.Add(dateDto);
//            courseDto.PresentList = dateDtoList;
//            var hworkDto = new HomeworkDto();
//            hworkDto.Id = 1;
//            hworkDto.MaxPoints = 100;
//            hworkDto.Points = 90;
//            hworkDto.Student = studentDto;
//            var hworkDtoList = new List<HomeworkDto>();
//            hworkDtoList.Add(hworkDto);
//            courseDto.HomeworkList = hworkDtoList;

//            var course = new Course();
//            course.Id = 1;
//            course.Name = "Test Raportu";
//            course.Leader = "Ja";
//            course.StartDate = DateTime.Today;
//            course.MinHomework = 80;
//            course.MinPresent = 80;
//            course.MaxStudents = 12;
//            course.StudentList = new List<Student>();
//            var student = new Student();
//            student.Id = 1;
//            student.Name = "John";
//            student.Surname = "Nowak";
//            student.Pesel = 123;
//            student.BirthDate = DateTime.Parse("12.12.2000");
//            student.Sex = 2;
//            course.StudentList.Add(student);
//            var student2 = new Student();
//            student2.Name = "Piotr";
//            student2.Surname = "Mazur";
//            course.StudentList.Add(student2);
//            var date = new Present();
//            date.Id = 1;
//            date.Date = DateTime.Today;
//            date.IsPresent = 1;
//            date.Student = student;
//            var dateList = new List<Present>();
//            dateList.Add(date);
//            course.PresentList = dateList;
//            var hwork = new Homework();
//            hwork.Id = 1;
//            hwork.MaxPoints = 100;
//            hwork.Points = 90;
//            hwork.Student = student;
//            var hworkList = new List<Homework>();
//            hworkList.Add(hwork);
//            course.HomeworkList = hworkList;

//            StringBuilder str = new StringBuilder();
//            str.Append("Nazwa kursu: ").Append(courseDto.Name).AppendLine();
//            str.Append("Data rozpoczęcia kursu: ").Append(courseDto.StartDate.ToString("d")).AppendLine();
//            str.Append("Minimalny próg obecności: ").Append(courseDto.MinPresent).Append("%").AppendLine();
//            str.Append("Minimalny próg pracy domowej: ").Append(courseDto.MinHomework).Append("%").AppendLine();
//            str.Append("Podsumowanie obecności:");
//            str.AppendLine();
//            str.Append(student.Name).Append(" ").Append(student.Surname).Append(" ").Append(1).Append("/").Append(1).Append(" (")
//                .Append(100).Append(") - ").Append("Zaliczone").AppendLine();
//            str.Append("Brak sprawdzonej obecności dla ").Append(student2.Name).Append(" ").Append(student2.Surname).AppendLine();
//            str.Append("Podsumowanie pracy domowej:");
//            str.AppendLine();
//            str.Append(student.Name).Append(" ").Append(student.Surname).Append(" ")
//                .Append(90).Append("/").Append(100).Append(" (").Append(90)
//                .Append(") - ").Append("Zaliczone").AppendLine();
//            str.Append("Brak sprawdzonej pracy domowej dla ").Append(student2.Name).Append(" ").Append(student2.Surname).AppendLine();
//            str.ToString();

//            var courseRepositoryMock = new Mock<IGenericRepository>();
//            courseRepositoryMock.Setup(x => x.GetCourseByIdFromBase(course.Id)).Returns(course);
//            var raportService = new RaportServices(courseRepositoryMock.Object);
//            var result = raportService.TakeDataToRaport(courseDto.Id);

//            Assert.AreEqual(result,str);
            
//        }
//    }
//}
