﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Models;

namespace DataLayer.DbContexts
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base(GetConnectionString())
        {
        }

        public DbSet<Course> CoursesDbSet { get; set; }
        public DbSet<Student> StudentsDbSet { get; set; }
        public DbSet<Present> PresentsDbSet { get; set; }
        public DbSet<Homework> HomeworksDbSet { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
        }
    }
}

