﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.DbContexts;
using DataLayer.Models;
using DataLayer.Repositories;

namespace DataLayer.Services
{
    public class GenericRepositoryServices<T> where T : class
    {

        public List<Course> GetCourseListIncludedStudents()
        {
            var unitOfWork = GenericRepositoryUnitOfWork.GetInstance();
            var repo = unitOfWork.GetRepository<Course>();
            return repo.GetAll().Include(a => a.StudentList).ToList();
        }
        

        public Course GetCourseByIdIncludedAllLists(int Id)
        {
            var unitOfWork = GenericRepositoryUnitOfWork.GetInstance();
            var repo = unitOfWork.GetRepository<Course>();
            var course = repo.GetAll();
                return course.Include
                (a => a.StudentList).Include(b => b.HomeworkList)
                .Include(c => c.PresentList).Single(a => a.Id == Id);
        }


    }
}
