﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.DbContexts;
using DataLayer.Interfaces;

namespace DataLayer.Repositories
{
    public class GenericRepository <T> where T : class , IGenericRepository
    {
        private ApplicationDbContext _dbContext;
        private DbSet<T> _repo => _dbContext.Set<T>();

        public GenericRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<T> GetAll()
        {
            return _repo;
        }

        public T GetById(int id)
        {
            return GetAll().SingleOrDefault(a => a.Id == id);
        }

        public bool Add(T repo)
        {
            _repo.Add(repo);
            return true;
        }

        public bool Update(T repo)
        {
            _repo.Attach(repo);
            return true;
        }
        

    }
}
