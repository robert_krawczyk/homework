﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using DataLayer.DbContexts;
using DataLayer.Interfaces;
using DataLayer.Models;

namespace DataLayer.Repositories
{
    public class GenericRepositoryUnitOfWork
    {
        private ApplicationDbContext _dbContext;
        private Dictionary<Type, object> _repositories;

        private static GenericRepositoryUnitOfWork _instance;

        public static GenericRepositoryUnitOfWork GetInstance()
        {
            if (_instance == null)
            {
                _instance = new GenericRepositoryUnitOfWork();
            }
            return _instance;
        }

        private GenericRepositoryUnitOfWork()
        {
            _dbContext = new ApplicationDbContext();
            RegisterRepositories();
        }

        private void RegisterRepositories()
        {
            _repositories = new Dictionary<Type, object>();

            Register(typeof(Course), new GenericRepository<Course>(_dbContext));
            Register(typeof(Student), new GenericRepository<Student>(_dbContext));
        }

        private void Register(Type type, object genericRepository)
        {
            if (_repositories.ContainsKey(type))
            {
                throw new Exception("There is a repository for type " + type.Name);
            }
            _repositories.Add(type, genericRepository);
        }

        public GenericRepository<T> GetRepository<T>() where T : class, IGenericRepository
        {
            return _repositories[typeof(T)] as GenericRepository<T>;
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        
}

    }

    


