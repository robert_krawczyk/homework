﻿namespace DataLayer.Interfaces

{
    public interface IGenericRepository
    {
        int Id { get; set; }
    }
}