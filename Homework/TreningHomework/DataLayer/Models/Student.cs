﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Interfaces;
using DataLayer.Repositories;

namespace DataLayer.Models
{
    public class Student : IGenericRepository
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public long Pesel { get; set; }
        public DateTime BirthDate { get; set; }
        public int Sex { get; set; }
    }
}
