﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Interfaces;
using DataLayer.Repositories;

namespace DataLayer.Models
{
    public class Course : IGenericRepository
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Leader { get; set; }
        public DateTime StartDate { get; set; }
        public double MinHomework { get; set; }
        public double MinPresent { get; set; }
        public int MaxStudents { get; set; }
        public List<Student> StudentList { get; set; }
        public List<Present> PresentList { get; set; }
        public List<Homework> HomeworkList { get; set; }
    }
}
