﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public Student Student { get; set; }
        public double MaxPoints { get; set; }
        public double Points { get; set; }

    }
}
