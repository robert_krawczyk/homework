﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Present
    {
        public int Id { get; set; }
        public Student Student { get; set; }
        public DateTime Date { get; set; }
        public double IsPresent { get; set; }
    }
}
