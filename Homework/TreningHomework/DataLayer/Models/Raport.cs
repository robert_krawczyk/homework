﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Raport
    {
        public Course course { get; set; }
        public List<Student> students { get; set; }
        public List<Homework> homeworks {get; set;  }
        public List<Present> presents { get; set;  }
    }
}
