﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusisnessLayer.ModelsDto;
using Newtonsoft.Json;

namespace RaportReader
{
    class Program
    {
        static void Main(string[] args)
        {
            var raport = new Program().Run();
            double procent = 0;
            double sum = 0;
            Console.WriteLine("Nazwa kursu: " + raport.course.Name);
            Console.WriteLine("Prowadzący: " + raport.course.Leader);
            foreach (var log in raport.homeworks)
            {
                procent = procent + log.ProcentOfHomework;
                sum++;
            }
            Console.WriteLine("Średni % z prac domowych : " + Convert.ToInt32(procent / sum));
            foreach (var log in raport.presents)
            {
                procent = procent + log.ProcentOfPresent;
                sum++;
            }
            Console.WriteLine("Średni % z obecności : " + Convert.ToInt32(procent / sum));

            foreach (var log in raport.presents)
            {
                if (log.pass == "zaliczone")
                {
                    sum++;

                }
            }
            Console.WriteLine("Obecność zaliczyło: " + sum + " osób");

            foreach (var log in raport.homeworks)
            {
                if (log.pass == "zaliczone")
                {
                    sum++;

                }
            }
            Console.WriteLine("Pracę domową zaliczyło: " + sum + " osób");


        }

        public RaportDto Run()
            {
                Console.WriteLine("Proszę podać ścieżkę dostępu i nazwę pliku");
                var filepath = Console.ReadLine();
                var stringfile = File.ReadAllText(filepath);
                var file = JsonConvert.DeserializeObject<RaportDto>(stringfile);
                return file;
            }
    }
}
