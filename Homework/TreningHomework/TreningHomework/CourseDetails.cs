﻿using System;
using System.Collections.Generic;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using BusisnessLayer.Services;
using DataLayer.Repositories;
using TreningHomework.Interfaces;
using TreningHomework.Menus;

namespace TreningHomework
{
    public class CourseDetails
    {
        private IInputQuestions _ask;
        private IStudentServices _studentServices;
        private ICourseServices _courseServices;
        private IRaportServices _raportServices;
        private IInputTypeCheck _type;
        private ICommandMenus _command;
        private IEntityToDtoMappers _entityToDtoMappers;
        private string _typedCommand;

        public CourseDetails(ICourseServices courseServices, IStudentServices studentServices,
            IRaportServices raportServices,IInputQuestions ask, IInputTypeCheck type, ICommandMenus command,IEntityToDtoMappers entityToDtoMappers)
        { 
        _courseServices = courseServices;
        _studentServices = studentServices;
        _raportServices = raportServices;
        _type = type;
        _command = command;
        _ask = ask;
        _entityToDtoMappers = entityToDtoMappers;
        }
        private CommandDispatcher _commandDispatcher;
        
        public void Menu()
        {
            
            var _comandArgs = new CourseMenuHandlerArgs(_courseServices, _studentServices, _raportServices, _ask, _type,
                _command, _entityToDtoMappers);
            _commandDispatcher = new CommandDispatcher();
            _commandDispatcher.AddCommand("1", _comandArgs.StudentAddToCourseMethod);
            _commandDispatcher.AddCommand("2", _comandArgs.CourseEditMethod);
            _commandDispatcher.AddCommand("3", _comandArgs.HomeWorkCheckMethod);
            _commandDispatcher.AddCommand("4", _comandArgs.PresentCheckMethod);
            _commandDispatcher.AddCommand("5", _comandArgs.RaportShowMethod);
            _commandDispatcher.AddCommand("6", _comandArgs.ReturnMethod);


            while (true)
            {
                TextMenus.CourseMenu();
                _typedCommand = _type.GetString("");
                if (!_commandDispatcher.CheckIfCommandExists(_typedCommand))
                {
                    Console.WriteLine("Wpisana komenda nie istnieje");
                }
                else
                {
                    _commandDispatcher.ExecuteCommand(_typedCommand);

                }
            }
        }

        public int CourseSelect()
        {
            var courseList = _courseServices.GetCoursesList();
            _command.CourseSelect();
            foreach (var log in courseList)
            {
                Console.WriteLine(log.Id + " " + log.Name);
            }
            var courseId = _type.GetInt("");
            return courseId;
        }

        public void StudentAddToCourse(int courseId)
        {
            var course = _courseServices.GetCourseById(courseId);
            var studentList = _studentServices.GetStudentsList();


            foreach (var student in studentList)
            {
                if (_ask.AskForAdd(student.Name + " " + student.Surname) == true)
                {
                    course.StudentList.Add(student);
                }
            }
            Test(_courseServices.UpdateCourse(course));
        }

        public void CourseEdit(int courseId)
        {
            var course = _courseServices.GetCourseById(courseId);

            if (_ask.AskForChanges("nazwę kursu") == true)
            {
                course.Name = _ask.GetCourseName();
            }
            if (_ask.AskForChanges("dane prowadzącego") == true)
            {
                course.Leader = _ask.GetLeaderDetails();
            }
            if (_ask.AskForChanges("minimalny % pracy domowej") == true)
            {
                course.MinHomework = _ask.GetMinHworkValue();
            }
            if (_ask.AskForChanges(" minilany % obecności") == true)
            {
                course.MinPresent = _ask.GetMinPrsentValue();
            }

            List<StudentDto> toRemove = new List<StudentDto>();

            foreach (var student in course.StudentList)
            {
                if (_ask.AskForDelete(student.Name + " " + student.Surname) == true)
                {
                    toRemove.Add(student);
                }
            }
            foreach (var student in toRemove)
            {
                course.StudentList.Remove(student);
            }
            Test(_courseServices.UpdateCourse(course));
        }

        public void HomeworkCheck(int courseId)
        {
            var course = _courseServices.GetCourseById(courseId);

            foreach (var student in course.StudentList)
            {
                    HomeworkDto _a = new HomeworkDto();
                    _command.StudentName(student);
                    _a.MaxPoints = _ask.GetMaxPoints();
                    _a.Points = _ask.GetPoints(_a.MaxPoints);
                    _a.Student = student;
                    course.HomeworkList.Add(_a);
            }
            Test(_courseServices.UpdateCourse(course));
        }

        public void PresentCheck(int courseId)
        {
            var course = _courseServices.GetCourseById(courseId);
            var _day = _ask.GetDate();
            foreach (var student in course.StudentList)
            {
                PresentDto _a = new PresentDto();
                _command.StudentName(student);
                _a.Date = _day;
                _a.IsPresent = _ask.GetPresentStatus();
                _a.Student = student;
                course.PresentList.Add(_a);
            }
            Test(_courseServices.UpdateCourse(course));
        }

        public void Test(bool test)
        {
            if (test == true)
            {
                Console.Clear();
                _command.Success();
            }
            else
            {
                Console.Clear();
                _command.Error();
            }
        }

        public void RaportShow(int Id)
        {
            _raportServices.TakeDataToRaport(Id);
        }
    }
    
}

