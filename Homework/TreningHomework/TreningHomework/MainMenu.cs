﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using TreningHomework.Interfaces;
using TreningHomework.Menus;

namespace TreningHomework
{
    public class MainMenu
    {
        
        private IInputQuestions _ask;
        private IStudentServices _studentServices;
        private ICourseServices _courseServices;
        private IRaportServices _raportServices;
        private IInputTypeCheck _type;
        private ICommandMenus _command;
        private IEntityToDtoMappers _entityToDtoMappers;
        private string _typedcommand;
        private CourseDto _course;

        public MainMenu(ICourseServices courseServices, IStudentServices studentServices,
            IRaportServices raportServices,IInputQuestions ask, IInputTypeCheck type,
            ICommandMenus command,IEntityToDtoMappers entityToDtoMappers)
        {
            _courseServices = courseServices;
            _studentServices = studentServices;
            _raportServices = raportServices;
            _ask = ask;
            _type = type;
            _command = command;
            _entityToDtoMappers = entityToDtoMappers;
        }
        private CommandDispatcher _commandDispatcher;
        
        public void Run()
        {
            var _comandArgs = new MainMenuHandlerArgs(_courseServices, _studentServices, _raportServices, _ask, _type, _command, _entityToDtoMappers);
            _commandDispatcher = new CommandDispatcher();
            _commandDispatcher.AddCommand("1", _comandArgs.CourseAddMethod);
            _commandDispatcher.AddCommand("2", _comandArgs.CourseChooseMethod);
            _commandDispatcher.AddCommand("3", _comandArgs.StudentAddMethod);
            _commandDispatcher.AddCommand("6", _comandArgs.StudentChooseMethod);
            _commandDispatcher.AddCommand("5", _comandArgs.ReturnMethod);

            while (true)
            {
                TextMenus.MainMenu();
                _typedcommand = _type.GetString("");
                if (!_commandDispatcher.CheckIfCommandExists(_typedcommand))
                {
                    Console.WriteLine("Wpisana komenda nie istnieje");
                }
                else
                {
                    _commandDispatcher.ExecuteCommand(_typedcommand);

                }
            }
        }



        public void CourseAdd()
        {
            
            _course = new CourseDto();
                _course.Name = CheckNameByRegex();
                _course.Leader = _ask.GetLeaderDetails();
                _course.StartDate = _ask.GetCourseStartDate();
                _course.MinHomework = _ask.GetMinHworkValue();
                _course.MinPresent = _ask.GetMinPrsentValue();
                _course.MaxStudents = _ask.GetMaxStudentValue();
                _course.PresentList = new List<PresentDto>();
                _course.StudentList = new List<StudentDto>();
                _course.HomeworkList = new List<HomeworkDto>();

                Test(_courseServices.CourseAdd(_course));

            
        }

        private string CheckNameByRegex()
        {
            Regex regex = new Regex(@"C#_\w++_[A-Z]{2}");
            while (true)
            {
                var name = _ask.GetCourseName();

                if (regex.IsMatch(name) == true)
                {
                    return name;
                }
                Console.Clear();
                _command.CourseNameError();

            }
        }

        public void StudentAdd()
        {
            var student = new StudentDto
            {
                Name = _ask.GetName(),
                Surname = _ask.GetSurname(),
                BirthDate = _ask.GetBirthDate(),
                Pesel = _ask.GetPesel(),
                Sex = _ask.GetSex()
            };

            Test(_studentServices.StudentAdd(student));

        }

        public void Test(bool test)
        {
            if (test == true)
            {
                Console.Clear();
                _command.Success();
            }
            else
            {
                Console.Clear();
                _command.Error();
            }
        }


       
    }
}



