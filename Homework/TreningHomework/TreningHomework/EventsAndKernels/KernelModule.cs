﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Repositories;
using Ninject;
using Ninject.Modules;
using BusisnessLayer.Interfaces;
using BusisnessLayer.Mappers;
using BusisnessLayer.Services;
using TreningHomework.InputHelpers;
using TreningHomework.Interfaces;
using TreningHomework.Menus;

namespace TreningHomework.EventsAndKernels
{
    public class KernelModule
    {
        public static IKernel KernelLoad()
        {
            var kernel = new StandardKernel();
            kernel.Bind<GenericRepositoryUnitOfWork>();
            kernel.Bind<ICourseServices>().To<CourseServices>();
            kernel.Bind<IRaportServices>().To<RaportServices>();
            kernel.Bind<IStudentServices>().To<StudentServices>();
            kernel.Bind<IInputQuestions>().To<InputQuestions>();
            kernel.Bind<IInputTypeCheck>().To<InputTypeCheck>();
            kernel.Bind<ICommandMenus>().To<CommandMenus>();
            kernel.Bind<IDtoToEntityMappers>().To<DtoToEntityMappers>();
            kernel.Bind<IEntityToDtoMappers>().To<EntityToDtoMappers>();
            
            
            return kernel;
        }
    }
}
