﻿using BusisnessLayer.ModelsDto;

namespace TreningHomework.Interfaces
{
    public interface ICommandMenus
    {
        void CourseDetials(CourseDto courseDto);
        void CourseSelect();
        void Error();
        void StudentName(StudentDto student);
        void StudentSelect();
        void Success();
        void WrongNumber();
        void CourseNameError();
    }
}