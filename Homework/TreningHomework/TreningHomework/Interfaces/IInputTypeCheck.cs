﻿using System;

namespace TreningHomework.Interfaces
{
    public interface IInputTypeCheck
    {
        DateTime GetDate(string message);
        double GetDouble(string message);
        int GetInt(string message);
        long GetLong(string message);
        string GetString(string message);
    }
}