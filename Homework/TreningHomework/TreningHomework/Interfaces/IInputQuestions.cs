﻿using System;

namespace TreningHomework.Interfaces
{
    public interface IInputQuestions
    {
        bool AskForAdd(string message);
        bool AskForChanges(string message);
        bool AskForDelete(string message);
        DateTime GetBirthDate();
        string GetCourseName();
        DateTime GetCourseStartDate();
        DateTime GetDate();
        string GetLeaderDetails();
        int GetMaxPoints();
        int GetMaxStudentValue();
        double GetMinHworkValue();
        double GetMinPrsentValue();
        string GetName();
        long GetPesel();
        int GetPoints(double maxPoints);
        int GetPresentStatus();
        int GetSex();
        int GetStudentId();
        string GetSurname();
        string AskForFileName(string message);
    }
}