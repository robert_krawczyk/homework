﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TreningHomework.Interfaces;

namespace TreningHomework.InputHelpers
{
    public class InputTypeCheck : IInputTypeCheck
    {

        public string GetString(string message)
        {
            Console.WriteLine(message);
            string inputtext = Console.ReadLine();
            return inputtext;
        }

        public int GetInt(string message)
        {
            int number;
            Console.WriteLine(message);
            while(!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("To nie jest liczba");
            }
            return number;
        }

        public double GetDouble(string message)
        {

            double number;
            Console.WriteLine(message);
            while (!double.TryParse(Console.ReadLine(), out number)) 
            {
                Console.WriteLine("To nie jest liczba");
            }
            return number;
        }

        public long GetLong(string message)
        {
            long number;
            Console.WriteLine(message);
            while(!long.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Tonie jest liczba");
            }
            return number;
        }

        public DateTime GetDate(string message)
        {
            DateTime date;
            Console.WriteLine(message);
            while (!DateTime.TryParse(Console.ReadLine(), out date))
            {
                Console.WriteLine("To nie jest prawidłowy format daty");
            }
            return date;
        }

    }
}
