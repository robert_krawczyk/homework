﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using TreningHomework.Interfaces;

namespace TreningHomework.InputHelpers
{
    public class InputQuestions : IInputQuestions
    {
       
        private InputTypeCheck _type = new InputTypeCheck();

        public string GetName()
        {
            string message = "Proszę podać imię";
            return _type.GetString(message);
        }

        public string GetSurname()
        {
            string message = "Proszę podać nazwisko";
            return _type.GetString(message);
        }

        public DateTime GetBirthDate()
        {
            string message = "Proszę podać datę urodzenia";
            return _type.GetDate(message);
        }

        public long GetPesel()
        {
            string message = "Proszę podać Pesel";
            return _type.GetLong(message);
        }

        public int GetSex()
        {
            string message = "Prosze wybrać płeć \n" +
                             "1. Kobieta \n" +
                             "2. Mężczyzna";
            int trynumber = 0;
            while (trynumber < 1 || trynumber > 2)
            {
                Console.WriteLine("Prosze wybrać 1 lub 2");
                trynumber = _type.GetInt(message);
            }
            return trynumber;
        }

        public string GetCourseName()
        {
            string message = "Proszę podać nazwę kursu";
            return _type.GetString(message);
        }

        public string GetLeaderDetails()
        {
            string message = "Proszę podać imię i nazwisko prowadzącego";
            return _type.GetString(message);
        }

        public DateTime GetCourseStartDate()
        {
            string message = "Prosze podać datę rozpoczęcia kursu";
            return _type.GetDate(message);
        }

        public double GetMinPrsentValue()
        {
            string message = "Prosze podać minimalny procent obecności";
            double trynumber = 0;
            while (trynumber < 1 || trynumber > 100)
            {
                Console.WriteLine("Prosze podać liczbe z zakresu 1 - 100 ");
                trynumber = _type.GetDouble(message);
            }
            return trynumber;
        }

        public double GetMinHworkValue()
        {
            string message = "Proszę podać minilany procent zaliczenia pracy domowej";
            double trynumber = 0;
            while (trynumber < 1 || trynumber > 100)
            {
                Console.WriteLine("Prosze podać liczbe z zakresu 1 - 100 ");
                trynumber = _type.GetDouble(message);
            }
            return trynumber;
        }

        public int GetMaxStudentValue()
        {
            string message = "Prosze podać maksymalną ilośc kursantów";
            return _type.GetInt(message);
        }

        public DateTime GetDate()
        {
            string message = "Prosze podać datę";
            return _type.GetDate(message);
        }

        public int GetPresentStatus()
        {
            string message = "Prosze wybrać czy obecny(a) \n" +
                             "1.TAK \n" +
                             "0. NIE";
            int trynumber = 3;
            while (trynumber < 0 || trynumber > 1)
            {
                Console.WriteLine("Prosze wybrać 1 lub 0");
                trynumber = _type.GetInt(message);
            }
            return trynumber;
        }

        public int GetMaxPoints()
        {
            string message = "Porsze podać maksymalną ilośc punktów";
            int trynumber = 0;
            while (trynumber < 1 || trynumber > 100)
            {
                Console.WriteLine("Prosze podać liczbe z zakresu 1 - 100 ");
                trynumber = _type.GetInt(message);
            }
            return trynumber;
        }

        public int GetPoints(double maxPoints)
        {
            string message = "Porsze podać zdobytą ilośc punktów";
            int trynumber = 0;
            while (trynumber < 1 || trynumber > maxPoints)
            {
                
                trynumber = _type.GetInt(message);
            }
            return trynumber;
        }

        public int GetStudentId()
        {
            string message = "Proszę wybrać ID studenta";

            return _type.GetInt(message);
        }

        public bool AskForChanges(string message)
        {
            var a = ("Jeśli chcesz zmienić " + message + " prosze wpisac 'TAK'");

            var b = _type.GetString(a);
            if (b == "TAK")
            {
                return true;
            }
            return false;
        }

        public bool AskForDelete(string message)
        {
            var a = ("Jeśli chcesz usunąć " + message + " proszę wpisac 'TAK'");

            var b = _type.GetString(a);
            if (b == "TAK")
            {
                return true;
            }
            return false;
        }

        public bool AskForAdd(string message)
        {
            var a = ("Jeśli chcesz dodać " + message + " proszę wpisac 'TAK'");

            var b = _type.GetString(a);
            if (b == "TAK")
            {
                return true;
            }
            return false;
        }

        public string AskForFileName(string message)
        {
            var fileName = _type.GetString(message);
            return fileName;
        }
    }
}
