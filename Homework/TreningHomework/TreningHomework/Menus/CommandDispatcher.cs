﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreningHomework.Menus
{
    public class CommandDispatcher
    {
        public delegate void CommandHandler();
        private readonly Dictionary<string, CommandHandler> _commands = new Dictionary<string, CommandHandler>();
        

        
        public bool CheckIfCommandExists(string command)
            {
                return _commands.ContainsKey(command);
            }

            public void AddCommand(string command, CommandHandler handler)
            {
                if (CheckIfCommandExists(command))
                {
                    throw new Exception("Command " + command + " already defined!");
                }

                _commands.Add(command, handler);
            }

            public void ExecuteCommand(string command)
            {
                if (!CheckIfCommandExists(command))
                {
                    throw new Exception("Command " + command + " not defined! Cannot execute!");
                }

                var handler = _commands[command];
                handler();
            }
      
    }
}
