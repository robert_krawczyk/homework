﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreningHomework.Menus
{
    public class TextMenus
    {
        public  static void MainMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Wybierz numer polecenia:");
            Console.WriteLine();
            Console.WriteLine("1. Dodać kurs");
            Console.WriteLine();
            Console.WriteLine("2. Wybrać kurs");
            Console.WriteLine();
            Console.WriteLine("3. Dodać kursanta");
            Console.WriteLine();
            Console.WriteLine("4. Wybrać kursanta");
            Console.WriteLine();
            Console.WriteLine("5. Koniec");
        }

        public static void CourseMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Wybierz numer polecenia:");
            Console.WriteLine();
            Console.WriteLine("1. Dodać kursanta do kursu");
            Console.WriteLine();
            Console.WriteLine("2. Edytować kurs.");
            Console.WriteLine();
            Console.WriteLine("3. Sprawdzić pracę domową.");
            Console.WriteLine();
            Console.WriteLine("4. Sprawdić obecnośc.");
            Console.WriteLine();
            Console.WriteLine("5. Wyświetlić raport.");
            Console.WriteLine();
            Console.WriteLine("6. Powrót");
        }
    }
}
