﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusisnessLayer.ModelsDto;
using TreningHomework.Interfaces;

namespace TreningHomework.Menus
{
    public class CommandMenus : ICommandMenus
    {

        public void Error()
        {
            Console.WriteLine("Coś poszło nie tak :(");
        }

        public void WrongNumber()
        {
            Console.WriteLine("Ta cyfra nie występuje w MENU");
        }

        public void Success()
        {
            Console.WriteLine("Udało się");
        }

        public void StudentSelect()
        {
            Console.WriteLine("Wybierz kursanta z listy podając jego ID");
        }

        public void CourseSelect()
        {
            Console.WriteLine("Wybierz kurs z listy podając jego ID");
        }

        public void StudentName(StudentDto student)
        {
            Console.WriteLine(student.Name + " " + student.Surname);
        }

        public void CourseDetials(CourseDto courseDto)
        {
            Console.WriteLine("Nazwa kursu: " + courseDto.Name);
            Console.WriteLine("Data rozpoczęcia kursu: " + courseDto.StartDate.ToString("d"));
            Console.WriteLine("Minimalny próg obecności: " + courseDto.MinPresent + "%");
            Console.WriteLine("Minimalny próg pracy domowej: " + courseDto.MinHomework + "%");
        }
        public void CourseNameError()
        {
            Console.WriteLine("Nazaw kursu musi zawierać format");
            Console.WriteLine("C#_<dowolna nazwa>_<dwie duże litery>");
        }
    }
}
