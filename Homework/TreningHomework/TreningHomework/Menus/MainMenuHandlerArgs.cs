﻿using BusisnessLayer.Interfaces;
using TreningHomework.Interfaces;

namespace TreningHomework.Menus
{
    public class MainMenuHandlerArgs
    {
        private IInputQuestions _ask;
        private IStudentServices _studentServices;
        private ICourseServices _courseServices;
        private IRaportServices _raportServices;
        private IInputTypeCheck _type;
        private ICommandMenus _command;
        private IEntityToDtoMappers _entityToDtoMappers;
        

        public MainMenuHandlerArgs(ICourseServices courseServices, IStudentServices studentServices,
            IRaportServices raportServices, IInputQuestions ask, IInputTypeCheck type,
            ICommandMenus command, IEntityToDtoMappers entityToDtoMappers)
        {
            _courseServices = courseServices;
            _studentServices = studentServices;
            _raportServices = raportServices;
            _ask = ask;
            _type = type;
            _command = command;
            _entityToDtoMappers = entityToDtoMappers;
        }
        public void CourseAddMethod()
        {
            new MainMenu(_courseServices, _studentServices, _raportServices, _ask, _type, _command, _entityToDtoMappers).CourseAdd();
        }

        public void CourseChooseMethod()
        {
            new CourseDetails(_courseServices, _studentServices, _raportServices, _ask, _type, _command, _entityToDtoMappers).Menu();
        }

        public void StudentAddMethod()
        {
            new MainMenu(_courseServices, _studentServices, _raportServices, _ask, _type, _command, _entityToDtoMappers).StudentAdd();
        }

        public void StudentChooseMethod()
        {
            new StudentDetails(_courseServices, _studentServices, _ask, _type, _command).Edit();
        }

        public void ReturnMethod()
        {
             return;
        }

       
    }
}