﻿using Ninject;
using TreningHomework.EventsAndKernels;
using TreningHomework.Menus;


namespace TreningHomework
{
    public class Program
    {
        

        static void Main()
        {
            var kernel = KernelModule.KernelLoad();
            kernel.Get<MainMenu>().Run();
        }
        
    }
}



// nie działa ją ATTACH w reposiotory, same dane w programie sie aktualizuja
// wyrzucone mock.object z testow