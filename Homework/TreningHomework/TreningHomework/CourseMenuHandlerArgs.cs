﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusisnessLayer.Interfaces;
using BusisnessLayer.ModelsDto;
using BusisnessLayer.Services;
using Castle.Core.Logging;
using DataLayer.Models;
using DataLayer.Repositories;
using Newtonsoft.Json;
using TreningHomework.EventsAndKernels;
using TreningHomework.Interfaces;

namespace TreningHomework
{
    public class CourseMenuHandlerArgs
    {

        private IInputQuestions _ask;
        private IStudentServices _studentServices;
        private ICourseServices _courseServices;
        private IRaportServices _raportServices;
        private IInputTypeCheck _type;
        private ICommandMenus _command;
        private IEntityToDtoMappers _entityToDtoMappers;
        private CourseDetails _courseDetails;
        

        public CourseMenuHandlerArgs(ICourseServices courseServices, IStudentServices studentServices,
            IRaportServices raportServices, IInputQuestions ask, IInputTypeCheck type,
            ICommandMenus command, IEntityToDtoMappers entityToDtoMappers)
        {
            _courseServices = courseServices;
            _studentServices = studentServices;
            _raportServices = raportServices;
            _ask = ask;
            _type = type;
            _command = command;
            _entityToDtoMappers = entityToDtoMappers;
            _courseDetails = new CourseDetails(_courseServices, _studentServices, _raportServices, _ask, _type,
                _command, _entityToDtoMappers);
        }
        
        public void StudentAddToCourseMethod()
        {
            var Id = _courseDetails.CourseSelect();
            _courseDetails.StudentAddToCourse(Id);
        }

        public void CourseEditMethod()
        {
            var Id = _courseDetails.CourseSelect();
            _courseDetails.CourseEdit(Id);
        }

        public void HomeWorkCheckMethod()
        {
            var Id = _courseDetails.CourseSelect();
            _courseDetails.HomeworkCheck(Id);
        }

        public void PresentCheckMethod()
        {
            var Id = _courseDetails.CourseSelect();
            _courseDetails.PresentCheck(Id);
        }

        public void RaportShowMethod()
        {
            _raportServices.TakeRaport += PrintRaport;
            _raportServices.TakeRaport += ExportToFile;
            var Id = _courseDetails.CourseSelect();
            _courseDetails.RaportShow(Id);
           
        }

        public void ReturnMethod()
        {
            return;
        }

        public void PrintRaport(object sender, RaportTakeEventArgs args)
        {
            double procent = 0;
            double sum = 0;
            Console.WriteLine("Nazwa kursu: " +args.raportText.course.Name);
            Console.WriteLine("Prowadzący: " + args.raportText.course.Leader);
            foreach (var log in args.raportText.homeworks)
            {
                procent = procent + log.ProcentOfHomework;
                sum++;
            }

            Console.WriteLine("Średni % z prac domowych : " + (procent/sum));
            foreach (var log in args.raportText.presents)
            {
                procent = procent + log.ProcentOfPresent;
                sum++;
            }
            Console.WriteLine("Średni % z obecności : " + (procent / sum));

            foreach (var log in args.raportText.presents)
            {
                if (log.pass == "zaliczone")
                {
                    sum++;
                }

            }
            Console.WriteLine("Obecność zaliczyło: " + sum + " osób");

            foreach (var log in args.raportText.homeworks)
            {
                if (log.pass == "zaliczone")
                {
                    sum++;

                }
            }
            Console.WriteLine("Pracę domową zaliczyło: " + sum + " osób");

            
        }

        public void ExportToFile(object sender, RaportTakeEventArgs args)
        {

            var fileName = _ask.AskForFileName("Prosze podać nazwę pliku");
            var json = JsonConvert.SerializeObject(args.raportText);
            File.WriteAllText(@fileName + ".json", json);
        }
    }
}
