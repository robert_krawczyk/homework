﻿using System;
using BusisnessLayer.Interfaces;
using TreningHomework.Interfaces;

namespace TreningHomework
{
    public class StudentDetails
    {
        private IInputQuestions _ask;
        private IStudentServices _studentServices;
        private ICourseServices _courseServices;
        private IInputTypeCheck _type;
        private ICommandMenus _command;
       

        public StudentDetails(ICourseServices courseServices, IStudentServices studentServices,
            IInputQuestions ask, IInputTypeCheck type, ICommandMenus command)
        {
            _courseServices = courseServices;
            _studentServices = studentServices;
            _type = type;
            _command = command;
            _ask = ask;
        }

       public void Edit()
        {
            var studentsList = _studentServices.GetStudentsList();
            var courseList = _courseServices.GetCoursesList();

            foreach (var student in studentsList)
            {
                _command.StudentName(student);
            }
            var _studentId = _ask.GetStudentId();

            var student2 = _studentServices.GetStudentById(_studentId);

            if (_ask.AskForChanges("imię") == true)
            {
                student2.Name = _ask.GetName();
            }
            if (_ask.AskForChanges("nazwisko") == true)
            {
                student2.Surname = _ask.GetSurname();
            }
            if (_ask.AskForChanges("datę urodzenia") == true)
            {
                student2.BirthDate = _ask.GetBirthDate();
            }
            if (_ask.AskForChanges("udział w kursie") == true)
            {
                Console.WriteLine("Kursant jest przypisany do nastepujących kursów:");
                foreach (var course in courseList)
                {
                    foreach (var log in course.StudentList)
                    {
                        if (log.Id == _studentId)
                        {
                            Console.WriteLine(course.Id + " " + course.Name);
                        }
                    }
                }
                var _a = _type.GetInt("Wybierz kurs z którego ma być usunięty");
              
                foreach (var course in courseList)
                {
                    foreach (var log in course.StudentList)
                    {
                        if (log.Id == student2.Id)
                        {
                            course.StudentList.Remove(student2);
                        }
                    }
                }
            }
            
            Test(_studentServices.UpdateStudent(student2));
        }

        public void Test(bool test)
        {
            if (test == true)
            {
                Console.Clear();
                _command.Success();
            }
            else
            {
                Console.Clear();
                _command.Error();
            }
        }
    }
}
//nie zapisuje edycji studenta