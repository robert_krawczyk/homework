﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProgramRedistibulables;

namespace CheckComapnyName
{

    public class CheckNames : IPlugins
        {
            public string Name => "Nazwy firm - wypisuje wszystkie dostępne nazwy firm (plugin)";

            public List<RaportFile> CalculationInOneYear(DateTime startDate, DateTime endDate, List<RaportFile> file)
            {
                var listOfCompanys = new List<RaportFile>();
                var ListOfAllYears = new List<RaportFile>();
                foreach (var log in file)
                {
                    if (log.Date >= startDate && log.Date <= endDate)
                    {
                        var company = listOfCompanys.SingleOrDefault(x => x.Name == log.Name);

                        if (company != null)
                        {
                            company.Value = 0;
                        }
                        else
                        {
                            listOfCompanys.Add(log);
                        }
                    }
                }
                ListOfAllYears.AddRange(listOfCompanys);
                return ListOfAllYears;
            }

            public List<RaportFile> SummaryOfAllYearsCalculation(List<RaportFile> ListOfAllYears)
            {
            var summaredList = new List<RaportFile>();
                var inputData = ListOfAllYears;
                List<RaportFile> summaryList = new List<RaportFile>();
                foreach (var log in inputData)
                {
                    var company = summaryList.SingleOrDefault(x => x.Name == log.Name);

                    if (company != null)
                    {
                        company.Value = 0;
                    }
                    else
                    {
                        summaryList.Add(log);
                    }
                }
                summaredList.AddRange(summaryList);
                return summaredList;
            }
        }
}
