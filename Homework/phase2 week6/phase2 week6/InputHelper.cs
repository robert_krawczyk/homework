﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace phase2_week6
{
    public class InputHelper
    {
        Texts _text = new Texts();

        public DateTime GetDate(string message)
        {
            {
                DateTime date;
                Console.WriteLine(message);
                while (!DateTime.TryParse(Console.ReadLine(), out date))
                {
                    _text.WrongDateFormat();
                }
                return date;
            }
        }

        public int GetInt(string message)
        {
            int number;
            Console.WriteLine(message);
            while (!int.TryParse(Console.ReadLine(), out number))
            {
                _text.WrongIntFormat();
            }
            return number;
        }

        public string GetString(string message)
        {
            Console.WriteLine(message);
            string inputtext = Console.ReadLine();
            return inputtext;
        }
    }
}
