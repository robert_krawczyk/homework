﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace phase2_week6
{
    public class Texts
    {
        public string text;

        public string MainMenu()
        {
            text = " Wybierz numer z listy \n"+
                "1. Pokaż dane dla wszsytkich firm \n"+
                "2. Pokaż dane dla wybranej firmy\n"+
                "3. Koniec";

            return text;
        }

        public string AskForCompanyName()
        {
            text = "Podaj nazwę firmy";
            return text;
        }

        public string StartDate()
        {
            text = "Podaj datę początkową";
            return text;
        }

        public string EndDate()
        {
            text = "podaj datę końcową";
            return text;
        }

        public void NoFile(int year)
        {
            Console.WriteLine("Nie mogę odnaleźć pliku z roku: " +year);
            
        }

        public void WrongNumber()
        {
            Console.WriteLine("Podano zły numer");
        }

        public void WrongDateFormat()
        {
            Console.WriteLine("To nie jest prawidłowy format daty");
        }

        public void WrongIntFormat()
        {
            Console.WriteLine("To nie jest liczba");
        }

        public void StartHigherThenEnd()
        {
            Console.WriteLine("Data początkowa jest większa niż końcowa");
        }
    }
}
