﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace phase2_week6
{
    public class LoopMenu
    {
        private static string _companyName = null;
        int choose = 1;
        private InputHelper _input = new InputHelper(); 
        private Texts _text = new Texts();

        public void Loop()
        {
            while (true)
            {
                choose = _input.GetInt(_text.MainMenu());
                switch (choose)
                {
                    case 1:
                        MainProgram(_companyName);
                        break;
                    case 2:
                        _companyName = _input.GetString(_text.AskForCompanyName());
                        MainProgram(_companyName);
                        break;
                    case 3:
                        return;
                    default:
                        _text.WrongNumber();
                        break;
                }
                choose = new int();
            }
        }

        public void MainProgram(string _companyName)
        {
            Lists.ListOfAllYears = new List<RaportFile>();
            Lists.SummaredList = new List<RaportFile>();
            DateTime startDate = _input.GetDate(_text.StartDate());
            Console.WriteLine();
            DateTime endDate = _input.GetDate(_text.EndDate());

            if (startDate >= endDate)
            {
                _text.StartHigherThenEnd();
                Console.WriteLine();
                return;
            }
            else
            {
                new CountingMethods().CountDataInFiles(startDate, endDate, _companyName);
                Console.WriteLine();
                new CountingMethods().CountAllData(Lists.ListOfAllYears);
                Console.WriteLine();
                ViewResults();
                Console.WriteLine();
            }
        }
        
        private void ViewResults()
        {
            foreach (var log in Lists.SummaredList)
            {
                    Console.WriteLine(log.Name + " " + log.Value);
            }
        }
    }
}
