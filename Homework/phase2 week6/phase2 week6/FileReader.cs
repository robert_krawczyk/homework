﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace phase2_week6
{
    public class FileReader
    {
        Texts _text = new Texts();

        public List<RaportFile> DeserializeFile(string filename)
        {
            var file = JsonConvert.DeserializeObject<List<RaportFile>>(File.ReadAllText(filename));
            return file;
        }

        public bool CheckFileExist(string filename, int year)
        {
            try
            {
                var file = JsonConvert.DeserializeObject<List<RaportFile>>(File.ReadAllText(filename));
                return true;
            }
            catch
            {
                _text.NoFile(year);
                return false;
            }
        }
    }
}
