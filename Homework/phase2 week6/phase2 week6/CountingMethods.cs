﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using phase2_week6;

namespace phase2_week6
{
    public class CountingMethods
    {
        public void CountDataInFiles(DateTime startDate, DateTime endDate, string companyName)
        {
            var startYear = startDate.Year;
            var endYear = endDate.Year;
            int baseYear = startYear;
            int probe = endYear - startYear + 1;

            var handles = new ManualResetEvent[probe];
            for (var i = 0; i < probe; i++)
            {
                handles[i] = new ManualResetEvent(false);
                var currentYear = baseYear + i;
                var currentHandle = handles[i];
                Action wrappedAction = () =>
                {
                    try
                    {
                        CountDataInFile(currentYear, startDate, endDate, companyName);
                    }
                    finally
                    {
                        currentHandle.Set();
                    }
                };
                ThreadPool.QueueUserWorkItem(x => wrappedAction());
            }
            foreach (var manualResetEvent in handles)
            {
                manualResetEvent.WaitOne();
            }
        }

        private void CountDataInFile(int year, DateTime startDate, DateTime endDate, string companyName)
        {
            var filename = @"..\..\..\HW3_DataSet\CompaniesRevenies_" + year + ".json";
            if (new FileReader().CheckFileExist(filename, year) == true)
            {
                var file = new FileReader().DeserializeFile(filename);
                var listOfCompanys = new List<RaportFile>();
                var ListForOneCompany = new List<RaportFile>();

                if (companyName != null)
                {
                    foreach (var log in file)
                    {
                        if (companyName == log.Name)
                        {
                            ListForOneCompany.Add(log);
                        }
                    }
                    file = ListForOneCompany;
                }

                foreach (var log in file)
                {
                    if (log.Date >= startDate && log.Date <= endDate)
                    {
                        var company = listOfCompanys.SingleOrDefault(x => x.Name == log.Name);

                        if (company != null)
                        {
                            company.Value += log.Value;
                        }
                        else
                        {
                            listOfCompanys.Add(log);
                        }
                    }
                }
                Lists.ListOfAllYears.AddRange(listOfCompanys);
            }
        }

        public void CountAllData(List<RaportFile> listOfAllYears)
        {
            var inputData = listOfAllYears;
            List<RaportFile> summaryList = new List<RaportFile>();
            foreach (var log in inputData)
            {
                var company = summaryList.SingleOrDefault(x => x.Name == log.Name);

                if (company != null)
                {
                    company.Value += log.Value;
                }
                else
                {
                    summaryList.Add(log);
                }
            }
            Lists.SummaredList.AddRange(summaryList);
        }
    }
}