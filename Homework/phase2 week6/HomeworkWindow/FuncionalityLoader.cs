﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ProgramRedistibulables;

namespace HomeworkWindow
{
    class FuncionalityLoader
    {
        public static List<IPlugins> PluginsList = new List<IPlugins>();

        public FuncionalityLoader()
        {
            AddDefaultMethods();
            AddPlugins();
        }

        private void AddDefaultMethods()
        {
            PluginsList.Add(new CountingSummary());
        }

        public static void AddPlugins()
        {
            var dllFileNames = GetDLLFileNames();
            var dllFile = GetLoadedAssiemblies(dllFileNames);
            var plugins = GetPluggedOperations(dllFile);
            FuncionalityLoader.PluginsList.AddRange(plugins);
        }
        public static IEnumerable<string> GetDLLFileNames()
        {
            const string path = @".\Plugins";

            string[] dllFileNames = { };
            if (Directory.Exists(path))
            {
                dllFileNames = Directory.GetFiles(path, "*.dll");
            }
            return dllFileNames;
        }

        public static IEnumerable<Assembly> GetLoadedAssiemblies(IEnumerable<string> dllFiles)
        {
            var assemblies = new List<Assembly>();
            foreach (var dllfile in dllFiles)
            {
                var assemblyName = AssemblyName.GetAssemblyName(dllfile);
                var assembly = Assembly.Load(assemblyName);
                assemblies.Add(assembly);
            }
            return assemblies;
        }

        public static IEnumerable<IPlugins> GetPluggedOperations(IEnumerable<Assembly> assemblies)
        {
            var pluginType = typeof(IPlugins);
            var pluginCalculations = new List<IPlugins>();

            foreach (var assembly in assemblies)
            {
                if (assembly == null)
                {
                    continue;
                }
                var types = assembly.GetTypes();
                var pluginTypes = types.Where(type =>
                    !type.IsInterface
                    && !type.IsAbstract
                    && type.GetInterface(pluginType.FullName) != null);
                foreach (var type in pluginTypes)
                {
                    pluginCalculations.Add((IPlugins)
                        Activator.CreateInstance(type));
                }
            }
            return pluginCalculations;
        }

        public IEnumerable<string> GetCalculationNames()
        {
            return PluginsList.Select(x => x.Name);
        }

        

    }
}
