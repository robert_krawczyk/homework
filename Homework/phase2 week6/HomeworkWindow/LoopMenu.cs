﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProgramRedistibulables;

namespace HomeworkWindow
{
    public class LoopMenu
    {
        public void ButtonStarter()
        {
            var startDate = Form1.startDatePicker.Value;
            var endDate = Form1.endDatePicker.Value;
            var pluginType = Form1.PluginsList_box.SelectedItem;
            Lists.Error = new List<string>();

            if (pluginType == null)
            {
                 Lists.Error.Add("Nie wybrano z listy co ma robić program.");
            }
            else
            {
                if (startDate >= endDate)
                {
                    Lists.Error.Add("Data początkowa jest większa od końcowej");

                }
                else
                {
                    var companyName = Form1.textBoxForComapnyName.Text;
                    if (companyName == "")
                    {
                        companyName = null;
                    }
                    ;
                    new LoopMenu().MainProgram(startDate, endDate, companyName);
                }
            }
            Form1.resultsBox.Clear();
            Form1.resultsBox.AppendText(ViewErrors());
            Form1.resultsBox.AppendText(ViewResults());
        }
        
        public void MainProgram(DateTime startDate, DateTime endDate, string companyName)
        {
            Lists.ListOfAllYears = new List<RaportFile>();
            Lists.SummaredList = new List<RaportFile>();
            
                new CountingMethod().CountDataInFiles(startDate, endDate, companyName);
                Console.WriteLine();
                new CountingMethod().CountAllData(Lists.ListOfAllYears);
        }

        private string ViewErrors()
        {
            var str = new StringBuilder();

            foreach (var log in Lists.Error)
            {
                str.Append(log).AppendLine();
            }

            return str.ToString();
        }

        private string ViewResults()
        {
            var str = new StringBuilder();

            foreach (var log in Lists.SummaredList)
            {
                if (log.Value != 0)
                {
                    str.Append(log.Name + ": " + log.Value).AppendLine();
                }
                else
                {
                    {
                        str.Append(log.Name).AppendLine();
                    }
                }
            }
            return str.ToString();
        }
    }
}
