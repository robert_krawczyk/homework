﻿namespace HomeworkWindow
{
    public partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startDate_lbl = new System.Windows.Forms.Label();
            this.endDate_lbl = new System.Windows.Forms.Label();
            startDatePicker = new System.Windows.Forms.DateTimePicker();
            endDatePicker = new System.Windows.Forms.DateTimePicker();
            textBoxForComapnyName = new System.Windows.Forms.TextBox();
            this.programStart_btn = new System.Windows.Forms.Button();
            this.askForCompanyName_lbl = new System.Windows.Forms.Label();
            resultsBox = new System.Windows.Forms.TextBox();
            this.PluginsList = new System.Windows.Forms.Label();
            PluginsList_box = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // startDate_lbl
            // 
            this.startDate_lbl.AutoSize = true;
            this.startDate_lbl.Location = new System.Drawing.Point(26, 36);
            this.startDate_lbl.Name = "startDate_lbl";
            this.startDate_lbl.Size = new System.Drawing.Size(122, 13);
            this.startDate_lbl.TabIndex = 0;
            this.startDate_lbl.Text = "Podaj datę początkową:";
            // 
            // endDate_lbl
            // 
            this.endDate_lbl.AutoSize = true;
            this.endDate_lbl.Location = new System.Drawing.Point(29, 71);
            this.endDate_lbl.Name = "endDate_lbl";
            this.endDate_lbl.Size = new System.Drawing.Size(108, 13);
            this.endDate_lbl.TabIndex = 1;
            this.endDate_lbl.Text = "Podaj datę końcową:";
            // 
            // startDatePicker
            // 
            startDatePicker.Location = new System.Drawing.Point(181, 36);
            startDatePicker.Name = "startDatePicker";
            startDatePicker.Size = new System.Drawing.Size(200, 20);
            startDatePicker.TabIndex = 3;
            startDatePicker.ValueChanged += new System.EventHandler(this.StartDatePicker_ValueChanged);
            // 
            // endDatePicker
            // 
            endDatePicker.Location = new System.Drawing.Point(181, 71);
            endDatePicker.Name = "endDatePicker";
            endDatePicker.Size = new System.Drawing.Size(200, 20);
            endDatePicker.TabIndex = 4;
            endDatePicker.ValueChanged += new System.EventHandler(this.EndDatePicker_ValueChanged);
            // 
            // textBoxForComapnyName
            // 
            textBoxForComapnyName.Location = new System.Drawing.Point(29, 124);
            textBoxForComapnyName.Name = "textBoxForComapnyName";
            textBoxForComapnyName.Size = new System.Drawing.Size(352, 20);
            textBoxForComapnyName.TabIndex = 5;
            // 
            // programStart_btn
            // 
            this.programStart_btn.Location = new System.Drawing.Point(161, 274);
            this.programStart_btn.Name = "programStart_btn";
            this.programStart_btn.Size = new System.Drawing.Size(75, 23);
            this.programStart_btn.TabIndex = 6;
            this.programStart_btn.Text = "Licz dane";
            this.programStart_btn.UseVisualStyleBackColor = true;
            this.programStart_btn.Click += new System.EventHandler(this.Button1_Click);
            // 
            // askForCompanyName_lbl
            // 
            this.askForCompanyName_lbl.AutoSize = true;
            this.askForCompanyName_lbl.Location = new System.Drawing.Point(32, 105);
            this.askForCompanyName_lbl.Name = "askForCompanyName_lbl";
            this.askForCompanyName_lbl.Size = new System.Drawing.Size(318, 13);
            this.askForCompanyName_lbl.TabIndex = 7;
            this.askForCompanyName_lbl.Text = "Jeśli chcesz dane tylko wybracnej firmy to wpisz poniżej jej nazwę:";
            // 
            // resultsBox
            // 
            resultsBox.Location = new System.Drawing.Point(29, 348);
            resultsBox.Multiline = true;
            resultsBox.Name = "resultsBox";
            resultsBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            resultsBox.Size = new System.Drawing.Size(352, 291);
            resultsBox.TabIndex = 8;
            resultsBox.TextChanged += new System.EventHandler(this.ResultsBox_TextChanged);
            // 
            // PluginsList
            // 
            this.PluginsList.AutoSize = true;
            this.PluginsList.Location = new System.Drawing.Point(148, 147);
            this.PluginsList.Name = "PluginsList";
            this.PluginsList.Size = new System.Drawing.Size(112, 13);
            this.PluginsList.TabIndex = 9;
            this.PluginsList.Text = "Lista dostępnych opcji";
            // 
            // PluginsList_box
            // 
            PluginsList_box.FormattingEnabled = true;
            PluginsList_box.Location = new System.Drawing.Point(29, 173);
            PluginsList_box.Name = "PluginsList_box";
            PluginsList_box.Size = new System.Drawing.Size(352, 95);
            PluginsList_box.TabIndex = 10;
            var p = new PluginsLoader();
            PluginsList_box.BeginUpdate();
            foreach (var name in p.GetCalculationNames())
            {
                PluginsList_box.Items.Add(name);
            }
            PluginsList_box.EndUpdate();
            PluginsList_box.Sorted = true;
            PluginsList_box.SelectedIndexChanged += new System.EventHandler(this.PluginsList_box_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 651);
            this.Controls.Add(PluginsList_box);
            this.Controls.Add(this.PluginsList);
            this.Controls.Add(resultsBox);
            this.Controls.Add(this.askForCompanyName_lbl);
            this.Controls.Add(this.programStart_btn);
            this.Controls.Add(textBoxForComapnyName);
            this.Controls.Add(endDatePicker);
            this.Controls.Add(startDatePicker);
            this.Controls.Add(this.endDate_lbl);
            this.Controls.Add(this.startDate_lbl);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label startDate_lbl;
        private System.Windows.Forms.Label endDate_lbl;
        private System.Windows.Forms.Button programStart_btn;
        private System.Windows.Forms.Label askForCompanyName_lbl;
        private System.Windows.Forms.Label PluginsList;
        public static System.Windows.Forms.DateTimePicker startDatePicker;
        public static System.Windows.Forms.DateTimePicker endDatePicker;
        public static System.Windows.Forms.TextBox textBoxForComapnyName;
        public static System.Windows.Forms.TextBox resultsBox;
        public static System.Windows.Forms.ListBox PluginsList_box;
    }
}

