﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using ProgramRedistibulables;


namespace HomeworkWindow
{
    public class CountingMethod
    {
        private IPlugins _count = FuncionalityLoader.PluginsList.SingleOrDefault 
            (x => x.Name == Form1.PluginsList_box.SelectedItem.ToString());

        public void CountDataInFiles(DateTime startDate, DateTime endDate, string companyName)
        {
            var startYear = startDate.Year;
            var endYear = endDate.Year;
            int baseYear = startYear;
            int probe = endYear - startYear + 1;

            var handles = new ManualResetEvent[probe];
            for (var i = 0; i < probe; i++)
            {
                var currentYear = baseYear + i;
                CountDataInFile(currentYear, startDate, endDate, companyName);
            }
        }

        private void CountDataInFile(int year, DateTime startDate, DateTime endDate, string companyName)
        {
            var filename = @"HW3_DataSet\CompaniesRevenies_" + year + ".json";
            if (new FileReader().CheckFileExist(filename, year) == true)
            {
                var file = new FileReader().DeserializeFile(filename);
                var ListForOneCompany = new List<RaportFile>();

                if (companyName != null)
                {
                    foreach (var log in file)
                    {
                        if (companyName == log.Name)
                        {
                            ListForOneCompany.Add(log);
                        }
                    }
                    file = ListForOneCompany;
                }
                
                Lists.ListOfAllYears = _count.CalculationInOneYear(startDate, endDate, file);

            }
        }

        public void CountAllData(List<RaportFile> listOfAllYears)
            {
                var inputData = listOfAllYears;
                Lists.SummaredList = _count.SummaryOfAllYearsCalculation(inputData);
        }
    }
}