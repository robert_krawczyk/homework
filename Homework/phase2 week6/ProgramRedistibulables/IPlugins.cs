﻿using System;
using System.Collections.Generic;

namespace ProgramRedistibulables
{
    public interface IPlugins
    {
        string Name { get; }
        List<RaportFile> CalculationInOneYear(DateTime startDate, DateTime endDate, List<RaportFile> file);
        List<RaportFile> SummaryOfAllYearsCalculation(List<RaportFile> ListOfAllYears);
    }
}