﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using HomeworkWindow;
using ProgramRedistibulables;


namespace Summary
{
    public class CountingSummary :IPlugins
    {
        public string Name => "Sumy - zlicza i sumuje wszsytkie dane dla danego okresu (plugin)";

        
        public List<RaportFile> CalculationInOneYear(DateTime startDate, DateTime endDate, List<RaportFile> file)
        {
            List<RaportFile> _listOfAllYears = new List<RaportFile>();
        var listOfCompanys = new List<RaportFile>();

            foreach (var log in file)
            {
                if (log.Date >= startDate && log.Date <= endDate)
                {
                    var company = listOfCompanys.SingleOrDefault(x => x.Name == log.Name);

                    if (company != null)
                    {
                        company.Value += log.Value;
                    }
                    else
                    {
                        listOfCompanys.Add(log);
                    }
                }
            }
            _listOfAllYears.AddRange(listOfCompanys);

            return _listOfAllYears;
        }

        
        public List<RaportFile> SummaryOfAllYearsCalculation(List<RaportFile> ListOfAllYears)
        {
            List<RaportFile> _summaredList = new List<RaportFile>();
            var inputData = ListOfAllYears;
            List<RaportFile> summaryList = new List<RaportFile>();
            foreach (var log in inputData)
            {
                var company = summaryList.SingleOrDefault(x => x.Name == log.Name);

                if (company != null)
                {
                    company.Value += log.Value;
                }
                else
                {
                    summaryList.Add(log);
                }
            }
            _summaredList.AddRange(summaryList);
            return _summaredList;
        }
    }
}