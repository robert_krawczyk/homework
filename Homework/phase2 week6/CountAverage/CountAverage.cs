﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeworkWindow;
using ProgramRedistibulables;

namespace Average
{
    public class CountAverage : IPlugins
    {
        public string Name
        {
            get => "Średnia - wylicza średnią wartość danych dla danego okresu (plugin)";
            set { throw new NotImplementedException(); }
        }

        public List<RaportFile> CalculationInOneYear(DateTime startDate, DateTime endDate, List<RaportFile> file)
        {
            List<RaportFile> _listOfAllYears = new List<RaportFile>();
            var listOfCompanys = new List<RaportFile>();

            foreach (var log in file)
            {
                if (log.Date >= startDate && log.Date <= endDate)
                {
                    var company = listOfCompanys.FirstOrDefault(x => x.Name == log.Name);
                    if (company == null)
                    {
                        company = log;
                        company.Value = Convert.ToInt32(file.Where(x => x.Name == log.Name).Average(x => x.Value));
                        listOfCompanys.Add(company);
                    }
                }
            }
            _listOfAllYears.AddRange(listOfCompanys);
            return _listOfAllYears;
        }

        public List<RaportFile> SummaryOfAllYearsCalculation(List<RaportFile> ListOfAllYears)
        {
            var summaryList = new List<RaportFile>();
            var inputData = ListOfAllYears;
            var summaredList = new List<RaportFile>();
            
            foreach (var log2 in inputData)
            {
                
                var company = summaryList.FirstOrDefault(x => x.Name == log2.Name);
                if (company == null)
                {
                    company = log2;
                    company.Value = Convert.ToInt32(inputData.Where(x => x.Name == log2.Name).Average(x => x.Value));
                    summaryList.Add(company);
                }
            }
            summaredList.AddRange(summaryList);
            return summaredList;
        }
    }
}
