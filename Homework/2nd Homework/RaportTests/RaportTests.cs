﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using BusinessLayer.Services;
using DataLayer.Models;
using DataLayer.Repository;
using DataLayer.Repository.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;


// odkomentowac całośc do testów
namespace BusinessLayer.Tests
{

    [TestClass]
    public class RaportTests
    {


        [TestMethod]
        public void CheckRaport_GiveAllImpornantData_ReturnRaport()
        {

            var courseList = new List<Course>();
            var course = new Course();
            course.Id = 1;
            course.Name = "Test Raportu";
            course.Leader = "Ja";
            course.Start = DateTime.Today;
            course.MinHomework = 80;
            course.MinPresent = 80;
            course.MaxStudent = 12;
            course.Students = new List<DataLayer.Models.Student>();
            var student = new DataLayer.Models.Student();
            student.Id = 1;
            student.Name = "John";
            student.Surname = "Nowak";
            student.Pesel = 123;
            student.BirthDate = DateTime.Parse("12.12.2000");
            student.Sex = 2;
           // student.Courses = new List<Course>();
            //student.Courses.Add(course);
            course.Students.Add(student);
            var date = new Date();
            date.Id = 1;
            date.Day = DateTime.Today;
            date.Present = 1;
            date.Student = student;
            //date.Course = course;
            var dateList = new List<Date>();
            dateList.Add(date);
            course.Dates = dateList;
            var hwork = new Homework();
            hwork.Id = 1;
            hwork.MaxPoints = 100;
            hwork.Points = 90;
            //hwork.Course = course;
            hwork.Student = student;
            var hworkList = new List<Homework>();
            hworkList.Add(hwork);
            course.Homeworks = hworkList;


            courseList.Add(course);

            double procentOfPresentValue = date.Present / 1 * 100;

            string pass = "Zaliczone";
            if (date.Present / 1 < course.MinPresent)
            {
                pass = "Niezaliczone";
            }

            string presentDetails = date.Present + 1 + procentOfPresentValue + pass;
            string pass2 = "Zaliczone";
            if (hwork.Points / hwork.MaxPoints < course.MinHomework)
            { pass2 = "Niezaliczone"; }
            double procentOfHworkValue = hwork.Points / hwork.MaxPoints * 100;
            string HworkDetails = hwork.Points + hwork.MaxPoints + procentOfHworkValue + pass2;


            string summary = course.Id + course.Name + course.Start + course.MinHomework + course.MinPresent +
                      student.Name +
                      " " + student.Surname + presentDetails + HworkDetails;

            var courseRepositoryMock = new Mock<ICourseRepository>();
            courseRepositoryMock.Setup(x => x.GetCourseById(1)).Returns(course);
            var raport = new RaportServices(new CourseServices(courseRepositoryMock.Object));
            var result = raport.Show(course.Id);

            Assert.AreEqual(result, summary);



        }
    }
}
