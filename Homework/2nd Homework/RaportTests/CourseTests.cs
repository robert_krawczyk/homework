﻿using System;
using System.Text;
using System.Collections.Generic;
using BusinessLayer.ModelsDto;
using BusinessLayer.Services;
using DataLayer.Models;
using DataLayer.Repository;
using DataLayer.Repository.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace BusinessLayer.Tests
{
    [TestClass]
    public class CourseTests
    {
        [TestMethod]
        public void AddCourseToDataLayer_GiveAllData_ReturnTrue()
        {
            
            var course = new CourseDto();
            course.Id = 1;
            course.Name = "Test Raportu";
            course.Leader = "Ja";
            course.Start = DateTime.Today;
            course.MinHomework = 80;
            course.MinPresent = 80;
            course.MaxStudent = 12;
            course.Students = new List<StudentDto>();
            var student = new StudentDto();
            student.Id = 1;
            student.Name = "John";
            student.Surname = "Nowak";
            student.Pesel = 123;
            student.BirthDate = DateTime.Parse("12.12.2000");
            student.Sex = 2;
            student.Courses = new List<CourseDto>();
            student.Courses.Add(course);
            course.Students.Add(student);
            var date = new DateDto();
            date.Id = 1;
            date.Day = DateTime.Today;
            date.Present = 1;
            date.Student = student;
            date.Course = course;
            var dateList = new List<DateDto>();
            dateList.Add(date);
            course.Dates = dateList;
            var hwork = new HomeworkDto();
            hwork.Id = 1;
            hwork.MaxPoints = 100;
            hwork.Points = 90;
            hwork.Course = course;
            hwork.Student = student;
            var hworkList = new List<HomeworkDto>();
            hworkList.Add(hwork);
            course.Homeworks = hworkList;
            
            course.Students.ForEach(s => s.Courses = null);
            course.Homeworks.ForEach(hw => hw.Course = null);
            course.Dates.ForEach(d => d.Course = null);
            
            var courseRepository = new Mock<ICourseRepository>();
            var courseServices = new CourseServices(courseRepository.Object);
            var result = courseServices.AddCourse(course);
            Assert.IsTrue(result);

        }

        [TestMethod]
        public void TakeCourseFromDataLayer_GiveAllData_ReturnTrue()
        {
            var courseList = new List<Course>();
            var course = new Course();
            course.Id = 1;
            course.Name = "Test Raportu";
            course.Leader = "Ja";
            course.Start = DateTime.Today;
            course.MinHomework = 80;
            course.MinPresent = 80;
            course.MaxStudent = 12;
            course.Students = new List<DataLayer.Models.Student>();
            var student = new DataLayer.Models.Student();
            student.Id = 1;
            student.Name = "John";
            student.Surname = "Nowak";
            student.Pesel = 123;
            student.BirthDate = DateTime.Parse("12.12.2000");
            student.Sex = 2;
            student.Courses = new List<Course>();
            student.Courses.Add(course);
            course.Students.Add(student);
            var date = new Date();
            date.Id = 1;
            date.Day = DateTime.Today;
            date.Present = 1;
            date.Student = student;
            date.Course = course;
            var dateList = new List<Date>();
            dateList.Add(date);
            course.Dates = dateList;
            var hwork = new Homework();
            hwork.Id = 1;
            hwork.MaxPoints = 100;
            hwork.Points = 90;
            hwork.Course = course;
            hwork.Student = student;
            var hworkList = new List<Homework>();
            hworkList.Add(hwork);
            course.Homeworks = hworkList;
            // zabiera procenty bo ineczej wyskakuje Stack
            course.Students.ForEach(s => s.Courses = null);
            course.Homeworks.ForEach(hw => hw.Course = null);
            course.Dates.ForEach(d => d.Course = null);
            //
            courseList.Add(course);




            var courseRepository = new Mock<ICourseRepository>();
            courseRepository.Setup(x => x.GetCourseById(course.Id)).Returns(course);
            var courseServices = new CourseServices(courseRepository.Object);
            var result = courseServices.TakeCourseById(course.Id);


            Assert.IsNotNull(result);
        }
        [TestMethod]
        public void AddCourseToDataLayer_GiveNull_ReturnFalse()
        {


            var course = new CourseDto();
            course = null;
            var courseRepository = new Mock<ICourseRepository>();
            var courseServices = new CourseServices(courseRepository.Object);
            var result = courseServices.AddCourse(course);
            Assert.IsFalse(result);

        }

        [TestMethod]
        public void TakeCourseFromDataLayer_GiveNull_ReturnFalse()
        {
            var courseList = new List<Course>();
            var course = new Course();
            courseList.Add(course);




            var courseRepository = new Mock<ICourseRepository>();
            var courseServices = new CourseServices(courseRepository.Object);
            var result = courseServices.TakeCourseById(course.Id);

            Assert.IsNull(result);
        }
    }
}
