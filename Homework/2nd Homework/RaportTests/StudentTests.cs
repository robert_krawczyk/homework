﻿using System;
using System.Text;
using System.Collections.Generic;
using BusinessLayer.ModelsDto;
using BusinessLayer.Services;
using DataLayer.Repository;
using DataLayer.Models;
using DataLayer.Repository.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Student = DataLayer.Models.Student;



namespace BusinessLayer.Tests
{

    [TestClass]
    public class StudentTests
    {

        [TestMethod]
        public void CheckStudentAdd_GiveStudentData_ReturntTrue()
        {
            var studentList = new List<StudentDto>();
            var course = new CourseDto();
            course.Id = 1;
            course.Name = "Test Raportu";
            course.Leader = "Ja";
            course.Start = DateTime.Today;
            course.MinHomework = 80;
            course.MinPresent = 80;
            course.MaxStudent = 12;
            var student = new StudentDto();
            student.Id = 1;
            student.Name = "John";
            student.Surname = "Nowak";
            student.Pesel = 123;
            student.BirthDate = DateTime.Parse("12.12.2000");
            student.Sex = 2;
            student.Courses = new List<CourseDto>();
            student.Courses.Add(course);


            studentList.Add(student);

            var studentRepository = new Mock<IStudentRepository>();
            var studentServices = new StudentServices(studentRepository.Object);
            var result = studentServices.AddStudent(student);
            Assert.IsNotNull(result);


        }


        [TestMethod]
        public void CheckGetStudent_GiveStudentDetails_ReturnTrue()
        {

            var course = new Course();
            course.Id = 1;
            course.Name = "Test Raportu";
            course.Leader = "Ja";
            course.Start = DateTime.Today;
            course.MinHomework = 80;
            course.MinPresent = 80;
            course.MaxStudent = 12;
            var student = new DataLayer.Models.Student();
            student.Id = 1;
            student.Name = "John";
            student.Surname = "Nowak";
            student.Pesel = 123;
            student.BirthDate = DateTime.Parse("12.12.2000");
            student.Sex = 2;
            


            var studentRepository = new Mock<IStudentRepository>();
            studentRepository.Setup(x => x.GetStudent(student.Id)).Returns(student);
            var studentServices = new StudentServices(studentRepository.Object);
            var result = studentServices.TakeStudent(student.Id);

            Assert.IsNotNull(result);

        }

        [TestMethod]
        public void AddStudentToDataLayer_GiveNull_ReturnFalse()
        {


            var student = new StudentDto();
            student = null;

            var studentRepository = new Mock<IStudentRepository>();
            var studentServices = new StudentServices(studentRepository.Object);
            var result = studentServices.AddStudent(student);
            Assert.IsFalse(result);

        }

        [TestMethod]
        public void TakeStudentFromDataLayer_GiveNull_ReturnFalse()
        {
            var studentList = new List<Student>();
            var student = new DataLayer.Models.Student();
            studentList.Add(student);
            
            var studentRepository = new Mock<IStudentRepository>();
            var studentServices = new StudentServices(studentRepository.Object);
            var result = studentServices.TakeStudent(student.Id);

            Assert.IsNull(result);
        }

    }
}
