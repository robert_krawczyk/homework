﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.DbContexts;
using DataLayer.Models;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DataLayer.Repository.Interfaces;

namespace DataLayer.Repository
{
    public class StudentRepository : IStudentRepository
    {
        public List<Student> GetStudentByPesel(long pesel)
        {
            List<Student> student = null;
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                student = dbContext.StudentsDbSet.Where(a => a.Pesel == pesel).ToList();
            }
            return student;
        }

        public bool AddStudent(Student student)
        {
           
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
               dbContext.StudentsDbSet.Add(student);
                dbContext.SaveChanges();
            }
            return true;

        }

        public Student GetStudent(int Id)

        {
            Student student = null;
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                student = dbContext.StudentsDbSet.Single(x=>x.Id == Id);
                
                return student;
            }
        }
        public List<Student> GetStudentsList()

        {
            List<Student> students = null;
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                students = dbContext.StudentsDbSet.ToList();

                return students;
            }
        }
        public List<Student> GetStudentsListToEdit()

        {
            List<Student> students = null;
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                students = dbContext.StudentsDbSet.Include(x=>x.Courses).ToList();
                students.ForEach(s=>s.Courses.ForEach(c=>c.Students = null));
                return students;
            }
        }

        public bool UpdateStudent(Student student)
        {
            
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                dbContext.StudentsDbSet.Attach(student);
                var manager = ((IObjectContextAdapter)dbContext).ObjectContext.ObjectStateManager;
                manager.ChangeObjectState(student, EntityState.Modified);

                dbContext.SaveChanges();
            }
            return true;
        }

        private string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
        }


    }
}


