﻿using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.Repository.Interfaces
{
    public interface IStudentRepository
    {
        bool AddStudent(Student student);
        Student GetStudent(int Id);
        List<Student> GetStudentByPesel(long pesel);
        List<Student> GetStudentsList();
        List<Student> GetStudentsListToEdit();
        bool UpdateStudent(Student student);
    }
}