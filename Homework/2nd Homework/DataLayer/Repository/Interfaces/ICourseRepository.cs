﻿using System.Collections.Generic;
using DataLayer.Models;

namespace DataLayer.Repository.Interfaces
{
    public interface ICourseRepository
    {
        bool AddCourse(Course course);
        Course GetCourseById(int Id);
        Course GetCourseByName(string name);
        List<Course> GetCoursesList();
        List<Course> GetCoursesListToEdit();
        bool UpdateCourse(Course course);
    }
}