﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.DbContexts;
using DataLayer.Models;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DataLayer.Repository
{
    public class DateRepository 
    {
       
       

        public bool UpdateDate(Date date)
        {
            
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                dbContext.DateDbSet.Attach(date);

                var manager = ((IObjectContextAdapter)dbContext).ObjectContext.ObjectStateManager;
                manager.ChangeObjectState(date, EntityState.Added);
                

                dbContext.SaveChanges();
                
            }
            return true;
        }

       

        private string GetConnectionString()
            {
                return ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
            }
       
    }
}
