﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.DbContexts;
using DataLayer.Models;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using DataLayer.Repository.Interfaces;

namespace DataLayer.Repository
{
    public class CourseRepository : ICourseRepository
    {
        public Course GetCourseByName(string name)
        {
            var course = new Course();
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
               course = dbContext.CoursesDbSet.SingleOrDefault(a => a.Name == name);
            }
            return course;
        }

        public bool AddCourse(Course course)
        {
            
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                dbContext.CoursesDbSet.Add(course);
                dbContext.SaveChanges();
            }
            return true;

        }


        public List<Course> GetCoursesList()
        {
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                List<Course> getCourses = null;
                getCourses = dbContext.CoursesDbSet.ToList();
                
                return getCourses;
            }
        }
        public List<Course> GetCoursesListToEdit()
        {
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                List<Course> getCourses = null;
                getCourses = dbContext.CoursesDbSet.Include(x=>x.Students).ToList();
                getCourses.ForEach(c => c.Students.ForEach(s => s.Courses = null));

                return getCourses;
            }
        }
        public Course GetCourseById(int Id)
        {
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                Course getCourse = null;
                getCourse = dbContext.CoursesDbSet.Include(a => a.Students).Include(b => b.Dates).Include(c => c.Homeworks).Single(x => x.Id == Id);
                getCourse.Students.ForEach(s => s.Courses = null);
                getCourse.Homeworks.ForEach(hw => hw.Course = null);
                getCourse.Dates.ForEach(d => d.Course = null);

                return getCourse;
            }
        }
       

        public bool UpdateCourse(Course course)
        {
            
            using (var dbContext = new StudentsDbContext(GetConnectionString()))
            {
                dbContext.CoursesDbSet.AddOrUpdate(course);
                


                dbContext.SaveChanges();
                
            }
            return true;
        }
       


        private string GetConnectionString()
            {
                return ConfigurationManager.ConnectionStrings["MyDatabase"].ConnectionString;
            }
       
    }
}
