﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using DataLayer.Models;

namespace DataLayer.DbContexts
{
    public class StudentsDbContext : DbContext
    {
            public StudentsDbContext(string connectionString)
                : base(connectionString)

            { }

            public DbSet<Course> CoursesDbSet { get; set; }
            public DbSet<Student> StudentsDbSet { get; set; }
            public DbSet<Date> DateDbSet { get; set; }
            public DbSet<Homework> HomeworksDbSet { get; set; }
    }
}
