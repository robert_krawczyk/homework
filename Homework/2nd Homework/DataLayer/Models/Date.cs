﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Date
    {
        public int Id { get; set; }
        public Course Course { get; set; }
        public Student Student { get; set; }
        public DateTime Day { get; set; }
        public double Present { get; set; }
        
    }
}
