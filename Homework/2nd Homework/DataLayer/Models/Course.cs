﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Course
    {
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Leader { get; set; }
        public DateTime Start { get; set; }
        public double MinPresent { get; set; }
        public double MinHomework { get; set; }
        public int MaxStudent { get; set; }
        public List<Student> Students { get; set; }
        public List<Date> Dates { get; set; }
        public List<Homework> Homeworks { get; set; }

        
        
    }
}
