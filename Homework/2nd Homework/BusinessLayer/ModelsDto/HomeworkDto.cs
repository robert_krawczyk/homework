﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.ModelsDto;
using BusinessLayer.Services;

namespace BusinessLayer.ModelsDto
{
    public class HomeworkDto
    {
        public int Id;
        public CourseDto Course;
        public StudentDto Student;
        public double Points;
        public double MaxPoints;
        //public double HworkValue;
        //public double MaxHworkValue;
        //public double procentOfHworkValue;
        //public string pass;


    }
}
