﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLayer.ModelsDto
{
    public class CourseDto 
    {

        public int Id;
        public string Name;
        public string Leader;
        public DateTime Start;
        public double MinPresent;
        public double MinHomework;
        public int MaxStudent;
        public List<StudentDto> Students;
        public List<DateDto> Dates;
        public List<HomeworkDto> Homeworks;


       
    }
}
