﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.ModelsDto;

namespace BusinessLayer.ModelsDto
{
    public class StudentDto
    {
        public int Id;
        public long Pesel;
        public string Name;
        public string Surname;
        public DateTime BirthDate;
        public int Sex;
        public List<CourseDto> Courses;


    }
}
