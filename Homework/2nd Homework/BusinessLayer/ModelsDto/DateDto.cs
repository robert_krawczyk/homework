﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.ModelsDto;
using BusinessLayer.Services;
using DataLayer.Models;

namespace BusinessLayer.ModelsDto
{
    public class DateDto
    {
        public int Id;
        public CourseDto Course;
        public StudentDto Student;
        public DateTime Day;
        public double Present;
        //public double presentValue;
        //public double MaxPresentValue;
        //public double procentOfPresentValue;
        //public string pass;

    }
}
