﻿namespace BusinessLayer.Interfaces
{
    public interface IRaportServices
    {
        string Show(int Id);
    }
}