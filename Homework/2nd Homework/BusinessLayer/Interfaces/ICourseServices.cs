﻿using System.Collections.Generic;
using BusinessLayer.ModelsDto;

namespace BusinessLayer.Interfaces
{
    public interface ICourseServices
    {
        bool AddCourse(CourseDto courseDto);
        CourseDto GetCourseByName(string name);
        CourseDto TakeCourseById(int Id);
        List<CourseDto> TakeCoursesList();
        List<CourseDto> TakeCoursesListToEdit();
        bool UpdateCourse(CourseDto courseDto);
    }
}