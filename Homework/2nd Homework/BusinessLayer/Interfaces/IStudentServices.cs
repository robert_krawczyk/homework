﻿using System.Collections.Generic;
using BusinessLayer.ModelsDto;

namespace BusinessLayer.Interfaces
{
    public interface IStudentServices
    {
        bool AddStudent(StudentDto studentDto);
        bool GetStudentByPesel(long pesel);
        StudentDto TakeStudent(int Id);
        List<StudentDto> TakeStudentsList();
        List<StudentDto> TakeStudentsListToEdit();
        bool UpdateStudent(StudentDto studentDto);
    }
}