﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Interfaces;
using BusinessLayer.Mappers;
using BusinessLayer.ModelsDto;
using DataLayer.Models;
using DataLayer.Repository;
using DataLayer.Repository.Interfaces;

namespace BusinessLayer.Services
{
    public class CourseServices : ICourseServices
    {
        private ICourseRepository _courseRepository;
        
        public CourseServices(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        public bool AddCourse(CourseDto courseDto)
        {

            var course = DtoToEntityMapper.CourseDtoToEntityModel(courseDto);
            
            _courseRepository.AddCourse(course);
            if (course == null)
            {
                return false;
            }
            return true;
        }
        public CourseDto TakeCourseById(int Id)
        {
            
            var course = _courseRepository.GetCourseById(Id);
            var result = EntityToDtoMappers.CourseEntityModelToDto(course);

            return result;
        }
        // do tego miejsca

        public bool UpdateCourse(CourseDto courseDto)
        {
            
            var course = DtoToEntityMapper.CourseDtoToEntityModel(courseDto);
            
            _courseRepository.UpdateCourse(course); 
            return true;

        }
        public CourseDto GetCourseByName(string name)
        {

            var course = _courseRepository.GetCourseByName(name);
            var result = EntityToDtoMappers.CourseEntityModelToDto(course);
            return result;
        }
        public List<CourseDto> TakeCoursesList()
        {
            
            var courses = _courseRepository.GetCoursesList();

            return EntityToDtoMappers.CourseListEntityModelToDto(courses);

        }
        public List<CourseDto> TakeCoursesListToEdit()
        {
            
            var courses = _courseRepository.GetCoursesListToEdit();

            return EntityToDtoMappers.CourseListEntityModelToDto(courses);

        }
       
      
    }

}
