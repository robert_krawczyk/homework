﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Interfaces;
using BusinessLayer.ModelsDto;
using DataLayer.Models;
using DataLayer.Repository;
using DataLayer.Repository.Interfaces;

namespace BusinessLayer.Services
{

    public class RaportServices : IRaportServices
    {
        private ICourseServices _service;

        public RaportServices(ICourseServices service)
        {
            _service = service;
        }


        public string presentDetails;
        public string HworkDetails;
        public string summary;

        public string Show(int Id)
        {
            var course = _service.TakeCourseById(Id);

            if (course.Students.Count != 0)
            {
                foreach (var log in course.Students)
                {
                    if (course.Dates.Count != 0)
                    {
                        double presentValue = 0;
                        double maxPresentValue = 0;
                        double procentOfPresentValue = 0;

                        foreach (var log2 in course.Dates)
                        {
                            if (log.Id == log2.Student.Id)
                            {
                                presentValue = presentValue + log2.Present;
                                maxPresentValue++;
                            }
                        }
                        if (maxPresentValue == 0)
                        {
                            break;
                        }
                        procentOfPresentValue = presentValue / maxPresentValue * 100;
                        string pass = "Zaliczone";
                        if (presentValue / maxPresentValue < course.MinPresent)
                        {
                            pass = "Niezaliczone";
                        }
                        presentDetails = presentValue + maxPresentValue + procentOfPresentValue + pass;
                    }
                    if (course.Homeworks.Count != 0)
                    {
                        double hworkValue = 0;
                        double maxHworkValue = 0;
                        double procentOfHworkValue = 0;
                        foreach (var log3 in course.Homeworks)
                        {
                            if (log.Id == log3.Student.Id)
                            {
                                hworkValue = hworkValue + log3.Points;
                                maxHworkValue = maxHworkValue + log3.MaxPoints;
                            }
                            if (maxHworkValue == 0)
                            {
                                break;
                                
                            }
                            procentOfHworkValue = hworkValue / maxHworkValue * 100;
                            Convert.ToInt32(hworkValue);
                            Convert.ToInt32(maxHworkValue);
                            Convert.ToInt32(procentOfHworkValue);
                            string pass2 = "Zaliczone";
                            if (hworkValue / maxHworkValue < course.MinHomework)
                            {
                                pass2 = "Niezaliczone";
                            }
                            HworkDetails = hworkValue + maxHworkValue + procentOfHworkValue + pass2;
                        }
                    }
                    summary = course.Id + course.Name + course.Start + course.MinHomework + course.MinPresent +
                                     log.Name +
                                     " " + log.Surname + presentDetails + HworkDetails;
                }
            }
            return summary;
        }
       

    }
}


