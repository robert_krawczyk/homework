﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Interfaces;
using BusinessLayer.Mappers;
using BusinessLayer.ModelsDto;
using DataLayer.Models;
using DataLayer.Repository;
using DataLayer.Repository.Interfaces;

namespace BusinessLayer.Services
{
    public class StudentServices : IStudentServices
    {
        
        private IStudentRepository _studentRepository;
        
        public StudentServices(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        
        
        public bool AddStudent(StudentDto studentDto)
        {
           var student = DtoToEntityMapper.StudentDtoToEntityModel(studentDto);
            
            
            if (student == null)
            {
                return false;
            }
            return _studentRepository.AddStudent(student);

        }

        public StudentDto TakeStudent(int Id)
        {
            
            var student = _studentRepository.GetStudent(Id);
            var result = EntityToDtoMappers.StudentEntityModelToDto(student);

            return result;
        }

        
        public List<StudentDto> TakeStudentsListToEdit()
        {
            
            var students = _studentRepository.GetStudentsListToEdit(); 
            return EntityToDtoMappers.StudentListEntityModelToDto(students);
            


        }
        public List<StudentDto> TakeStudentsList()
        {
            
            var students = _studentRepository.GetStudentsListToEdit();

           return EntityToDtoMappers.StudentListEntityModelToDto(students);
            


        }
        public bool UpdateStudent(StudentDto studentDto)
        {
           
            var student = DtoToEntityMapper.StudentDtoToEntityModel(studentDto);
            
            return _studentRepository.UpdateStudent(student); 
            

        }


        public bool GetStudentByPesel(long pesel)
        {
            
            var student = _studentRepository.GetStudentByPesel(pesel);


            if (student == null || student.Count == 0)
            {
                return false;
            }
            return true;

        }
    }
    }

