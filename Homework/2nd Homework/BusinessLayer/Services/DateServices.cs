﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Mappers;
using BusinessLayer.ModelsDto;
using DataLayer.Repository;

namespace BusinessLayer.Services
{
    public class DateServices
    {


        public bool UpdateDate(DateDto dateDto)
        {

            var date = DtoToEntityMapper.DateDtoToEntityModel(dateDto);
            var dateRepository = new DateRepository();
            dateRepository.UpdateDate(date);
            return true;

        }
    }
}
