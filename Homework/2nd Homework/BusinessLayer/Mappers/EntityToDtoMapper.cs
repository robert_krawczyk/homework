﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.ModelsDto;
using BusinessLayer.Services;
using DataLayer.Models;

namespace BusinessLayer.Mappers
{
    public class EntityToDtoMappers
    {
        public static CourseDto CourseEntityModelToDto(Course course)
        {
            if (course == null)
            {
                return null;
            }
            var courseDto = new CourseDto();
            courseDto.Id = course.Id;
            courseDto.Name = course.Name;
            courseDto.Leader = course.Leader;
            courseDto.MinPresent = course.MinPresent;
            courseDto.Start = course.Start;
            courseDto.MinHomework = course.MinHomework;
            courseDto.MaxStudent = course.MaxStudent;
            courseDto.Students = course.Students?.Select(a => StudentEntityModelToDto(a)).ToList();
            courseDto.Dates = course.Dates?.Select(b => DateEntityModelToDto(b)).ToList();
            courseDto.Homeworks = course.Homeworks?.Select(c => HomeworkEntityModelToDto(c)).ToList();

            return courseDto;
        }

        public static DateDto DateEntityModelToDto(Date date)
        {
            if (date == null)
            {
                return null;
            }

            var dateDto = new DateDto();
            dateDto.Id = date.Id;
            dateDto.Day = date.Day;
            dateDto.Present = date.Present;
            dateDto.Course = CourseEntityModelToDto(date.Course);
            dateDto.Student = StudentEntityModelToDto(date.Student);

            return dateDto;
        }

        public static HomeworkDto HomeworkEntityModelToDto(Homework homework)
        {
            if (homework == null)
            {
                return null;
            }

            var homeworkDto = new HomeworkDto();

            homeworkDto.Id = homework.Id;
            homeworkDto.MaxPoints = homework.MaxPoints;
            homeworkDto.Points = homework.Points;
            homeworkDto.Course = CourseEntityModelToDto(homework.Course);
            homeworkDto.Student = StudentEntityModelToDto(homework.Student);

            return homeworkDto;
        }

        public static StudentDto StudentEntityModelToDto(Student student)
        {
            if (student == null)
            {
                return null;
            }

            var studentDto = new StudentDto();
            studentDto.Id = student.Id;
            studentDto.Name = student.Name;
            studentDto.Surname = student.Surname;
            studentDto.BirthDate = student.BirthDate;
            studentDto.Pesel = student.Pesel;
            studentDto.Sex = student.Sex;
            // studentDto.Courses = student.Courses?.Select(d=>CourseEntityModelToDto(d)).ToList();

            return studentDto;
        }


        public static List<StudentDto> StudentListEntityModelToDto(List<Student> students)
        {
            if (students == null)
            {
                return null;
            }
            var studentDto = new List<StudentDto>();
            
            foreach (var s in students)
            {
                studentDto.Add(StudentEntityModelToDto(s));
            }
            return studentDto;
        }
        

        public static List<CourseDto> CourseListEntityModelToDto(List<Course> courses)
        {
            if (courses == null)
            {
                return null;
            }
            var courseDto = new List<CourseDto>();

            foreach (var c in courses)
            {
                courseDto.Add(CourseEntityModelToDto(c));
            }
            return courseDto;
        }
    }
}
