﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.ModelsDto;
using DataLayer.Models;
using BusinessLayer.Mappers;

namespace BusinessLayer.Mappers
{
    internal class DtoToEntityMapper
    {

        public static Course CourseDtoToEntityModel(CourseDto courseDto)
        {
            if (courseDto == null)
            {
                return null;
            }
            var course = new Course();
            course.Id = courseDto.Id;
            course.Name = courseDto.Name;
            course.Leader = courseDto.Leader;
            course.MinPresent = courseDto.MinPresent;
            course.MinHomework = courseDto.MinHomework;
            course.MaxStudent = courseDto.MaxStudent;
            course.Start = courseDto.Start;
            course.Students = courseDto.Students?.Select(a => StudentDtoToEntityModel(a)).ToList();
            course.Dates = courseDto.Dates?.Select(b => DateDtoToEntityModel(b)).ToList();
            course.Homeworks = courseDto.Homeworks?.Select(c => HomeworkDtotoEntityModel(c)).ToList();

            return course;

        }

        public static Date DateDtoToEntityModel(DateDto dateDto)
        {
            if (dateDto == null)
            {
                return null;
            }
            var date = new Date();
            date.Id = dateDto.Id;
            date.Day = dateDto.Day;
            date.Present = dateDto.Present;
            
            date.Course = CourseDtoToEntityModel(dateDto.Course);
            date.Student = StudentDtoToEntityModel(dateDto.Student);
            
            return date;
        }

        public static Homework HomeworkDtotoEntityModel(HomeworkDto homeworkDto)
        {

            if (homeworkDto == null)
            {
                return null;
            }

            var homework = new Homework();
            homework.Id = homeworkDto.Id;
            homework.MaxPoints = homeworkDto.MaxPoints;
            homework.Course =  CourseDtoToEntityModel(homeworkDto.Course); 
            homework.Student = StudentDtoToEntityModel(homeworkDto.Student);

            return homework;

        }

        public static Student StudentDtoToEntityModel(StudentDto studentsDto)
        {
            if (studentsDto == null)
            {
                return null;
            }

            var student = new Student();
            student.Id = studentsDto.Id;
            student.Name = studentsDto.Name;
            student.Surname = studentsDto.Surname;
            student.Pesel = studentsDto.Pesel;
            student.BirthDate = studentsDto.BirthDate;
            student.Sex = studentsDto.Sex;
            //student.Courses = studentsDto.Courses?.Select(d=> CourseDtoToEntityModel(d)).ToList();

            return student;
        }

    }
}