﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Runtime.InteropServices;

namespace CliLayer.Menus
{
    public class MenuCommands
    {
        public static void MainMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Wybierz co chcesz zrobić: ");
            Console.WriteLine("1. Przejdź do MENU kursów.");
            Console.WriteLine("2. Przejdź do MENU kursantów.");
            Console.WriteLine("3. Koniec.");
            Console.WriteLine();
        }

        public static void CourseMenu()
        {
            Console.WriteLine();
            Console.WriteLine("Wybierz co chcesz zrobić: ");
            Console.WriteLine("1. Dodaj kurs.");
            Console.WriteLine("2. Wybierz istniejący kurs.");
            Console.WriteLine("3. Wróć do poprzedniego MENU");
            Console.WriteLine();
        }


        public static void CourseDetails()
    {
        Console.Clear();
        Console.WriteLine("Wybierz co chcesz zrobić: ");
        Console.WriteLine("1. Edytuj kurs. ");
        Console.WriteLine("2. Sprawdź obecność." );
        Console.WriteLine("3. Sprawdź pracę domową.");
        Console.WriteLine("4. Raport z kursu. ");
        Console.WriteLine("5. Wróć do poprzedniego MENU.");
        Console.WriteLine();
    }

        public static void StudentMenu()
        {
            Console.Clear();
            Console.WriteLine("Wybierz co chcesz zrobić:");
            Console.WriteLine("1. Dodaj kursanta.");
            Console.WriteLine("2. Edytuj kursanta.");
            Console.WriteLine("3. Wróć do poprzedniego MENU.");
            Console.WriteLine();
        }
    }
}