﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CliLayer.Menus
{
    public class ErrorCommands
    {
       
            public static void Error()
            {
                Console.Clear();
                Console.WriteLine("Błędne dane.");
                Console.WriteLine("Wpisz ponownie.");
                Console.WriteLine();

            }

            public static void CourseExist()
            {
                Console.Clear();
                Console.WriteLine("Kurs już istnieje.");
                Console.WriteLine();
            }

            public static void WrongValue()
            {
                Console.Clear();
                Console.WriteLine("Nieprawidłowa wartość.");
                Console.WriteLine("Wpisz dane ponownie.");

            }

            public static void TooLow()
            {
                Console.Clear();
                Console.WriteLine("Zbyt niska wartość.");
                Console.WriteLine("Wpisz dane ponownie.");

            }

            public static void Success()
            {
                Console.Clear();
                Console.WriteLine("Dane zapisane poprawnie");
                Console.WriteLine();

            }

            public static void StudentExist()
            {
                Console.Clear();
                Console.WriteLine("Student o tym numerze Pesel już istnieje.");
                Console.WriteLine();

            }

            public static void DateConflict()
            {
                Console.Clear();
                Console.WriteLine("Dane dla tego dnia zostały już wprowadzone.");
                Console.WriteLine();

            }
    }
}