﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer;
using BusinessLayer.Interfaces;
using BusinessLayer.ModelsDto;
using BusinessLayer.Services;
using CliLayer.Menus;
using DataLayer;
using DataLayer.Repository;

namespace _2nd_Homework
{
    public class Student
    {
        private IStudentServices _studentServices = new StudentServices(new StudentRepository());

        public void Menu()
        {
            int choose;
            choose = 0;
            while (choose != 3)
            {
                MenuCommands.StudentMenu();
                try
                {
                    choose = int.Parse(Console.ReadLine());

                    switch (choose)
                    {
                        case 1:
                            Console.WriteLine();
                            new Student().Add();
                            break;
                        case 2:
                            Console.WriteLine();
                            new Student().Edit();
                            break;
                        case 3:
                            return;

                        default:

                            ErrorCommands.Error();
                            break;
                    }
                    choose = new int();
                }

                catch
                {
                    ErrorCommands.Error();
                    break;
                }
            }
        }

        public void Add()
        {
            var student = new StudentDto();
            
            student.Pesel = InputHelpers.InputHelper.InputHelpers.GetLong("Pesel");

            var success = _studentServices.GetStudentByPesel(student.Pesel);
            if (success == false)
            {
                
                student.Name = InputHelpers.InputHelper.InputHelpers.GetString("Imie");
                student.Surname = InputHelpers.InputHelper.InputHelpers.GetString("Nazwisko");
                student.BirthDate = InputHelpers.InputHelper.InputHelpers.GetDateTime("Data Urodzenia");
                Console.WriteLine("Płeć \n" +
                                  "1. Kobieta \n" +
                                  "2. Mężczyzna");
                student.Sex = int.Parse(Console.ReadLine());
                if (student.Sex < 1 || student.Sex > 2)
                {
                    ErrorCommands.WrongValue();
                    return;
                }

                _studentServices.AddStudent(student);
            }
            else
            {
                ErrorCommands.StudentExist();
            }
        }

        public void Edit()
        {
            var studentsList = _studentServices.TakeStudentsList();

            Console.WriteLine("Wybierz Id kursanta:");
            foreach (var log in studentsList)
            {
                Console.WriteLine(log.Id + " " + log.Name + " " + log.Surname);
            }
            var studentId = int.Parse(Console.ReadLine());


            foreach (var log2 in studentsList)
            {
                if (studentId == log2.Id)
                {
                    Console.WriteLine("Jeśli chcesz zmienić 'Imie' wpisz 'TAK'");
                    var a = Console.ReadLine();
                    if (a == "TAK")
                    {
                        string newName = InputHelpers.InputHelper.InputHelpers.GetString("Imie: " + log2.Name);

                        log2.Name = newName;
                    }

                    Console.WriteLine("Jeśli chcesz zmienić 'Nazwisko' wpisz 'TAK'");
                    var b = Console.ReadLine();
                    if (b == "TAK")
                    {
                        string newSurname =
                            InputHelpers.InputHelper.InputHelpers.GetString("Nazwisko: " + log2.Surname);

                        log2.Surname = newSurname;
                    }
                    Console.WriteLine("Jeśli chcesz zmienić 'Data Urodzenia' wpisz 'TAK'");
                    var c = Console.ReadLine();
                    if (c == "TAK")
                    {
                        Console.WriteLine("Data Urodzenia: " + log2.BirthDate);
                        var newBirthDate = Console.ReadLine();

                        if (newBirthDate != "")
                        {
                            var newBirthDate2 = DateTime.Parse(newBirthDate);
                            log2.BirthDate = newBirthDate2;
                        }

                        var toRemove = new List<CourseDto>();
                        foreach (var log3 in log2.Courses)
                        {
                            Console.WriteLine("Jeśli chcesz usunąc studenta z kursu wpisz 'TAK'");
                            Console.WriteLine(log3.Id + " " + log3.Name);
                            var choose = (Console.ReadLine());
                            if (choose == "TAK")
                            {

                                toRemove.Add(log3);
                            }

                        }
                        foreach (var item in toRemove)
                        {
                            log2.Courses.Remove(item);
                        }
                        _studentServices.UpdateStudent(log2);
                    }
                    _studentServices.UpdateStudent(log2);
                }
            }
        }
    }
}
