﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2nd_Homework.InputHelpers
{
    public class InputHelper
    {
        public class InputHelpers
        {
            public static int GetInt(string message)
            {
                
                int number;
                Console.WriteLine(message);
                while (!int.TryParse(Console.ReadLine(), out number))
                    {
                        Console.WriteLine("To nie jest liczba, spróbuj ponownie");
                    }
                    return number;
                
            }
            public static long GetLong(string message)
            {

                long number;
                Console.WriteLine(message);
                while (!long.TryParse(Console.ReadLine(), out number))
                {
                    Console.WriteLine("To nie jest liczba, spróbuj ponownie");
                }
                return number;

            }
            public static double GetDouble(string message)
            {
                double number;
                Console.WriteLine(message);
                while (!double.TryParse(Console.ReadLine(), out number))
                {
                    Console.WriteLine("To nie jest liczba, spróbuj ponownie");
                }
                return number;
            }
            public static DateTime GetDateTime(string message)
            {
               DateTime date;
                Console.WriteLine(message);
                while (!DateTime.TryParse(Console.ReadLine(), out date))
                    {
                        Console.WriteLine("To nie jest data, spróbuj ponownie");
                    }
                    return date;
            }
           
            public static string GetString(string message)
            {
                Console.WriteLine(message);
                message = Console.ReadLine();
                return message;
            }

        }
    }

}

