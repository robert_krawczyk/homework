﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Services;
using DataLayer.Models;
using DataLayer.Repository;
using DataLayer.Repository.Interfaces;
 
// zakomentować całośc do testów
namespace CliLayer
{
    public class Raport
    {


        public void Show(int Id)
        {

            var course = new CourseServices(new CourseRepository()).TakeCourseById(Id);
            Console.Clear();

            Console.WriteLine("Nazwa kursu: " + course.Name);
            Console.WriteLine("Data rozpoczęcia: " + course.Start);
            Console.WriteLine("Minimalny próg obecności: " + course.MinPresent * 100);
            Console.WriteLine("Minimlany prób pracy domowej: " + course.MinHomework * 100);
            Console.WriteLine();

            if (course.Students.Count != 0)
            {
                Console.WriteLine("Obecność");
                foreach (var log in course.Students)
                {
                    if (course.Dates.Count != 0)
                    {
                        double presentValue = 0;
                        double maxPresentValue = 0;
                        double procentOfPresentValue = 0;

                        foreach (var log2 in course.Dates)
                        {
                            if (log.Id == log2.Student.Id)
                            {
                                presentValue = presentValue + log2.Present;
                                maxPresentValue++;
                            }
                        }
                        procentOfPresentValue = presentValue / maxPresentValue * 100;

                        string pass = "Zaliczone";
                        if (presentValue / maxPresentValue < course.MinPresent)
                        {
                            pass = "Niezaliczone";
                        }
                        Convert.ToInt32(presentValue);
                        Convert.ToInt32(maxPresentValue);
                        Convert.ToInt32(procentOfPresentValue);
                        Console.WriteLine(log.Name + " " + log.Surname + " - "
                                          + presentValue + "/" + maxPresentValue
                                          + "(" + Convert.ToInt32(procentOfPresentValue) + "%) - " + pass);


                    }
                    else
                    {
                        Console.WriteLine(log.Name + " " + log.Surname + " - Brak sprawdzonej obecności");
                    }
                }
                Console.WriteLine("Praca Domowa");
                foreach (var log in course.Students)
                {
                    if (course.Homeworks.Count != 0)
                    {
                        double hworkValue = 0;
                        double maxHworkValue = 0;
                        double procentOfHworkValue = 0;
                        foreach (var log2 in course.Homeworks)
                        {
                            if (log.Id == log2.Student.Id)
                            {
                                hworkValue = hworkValue + log2.Points;
                                maxHworkValue = maxHworkValue + log2.MaxPoints;
                            }
                        }
                        procentOfHworkValue = hworkValue / maxHworkValue * 100;

                        string pass = "Zaliczone";
                        if (hworkValue / maxHworkValue < course.MinHomework)
                        {
                            pass = "Niezaliczone";
                        }
                        Convert.ToInt32(hworkValue);
                        Convert.ToInt32(maxHworkValue);
                        Convert.ToInt32(procentOfHworkValue);
                        Console.WriteLine(log.Name + " " + log.Surname + " - " + hworkValue + "/" +
                                              maxHworkValue
                                              + "(" + Convert.ToInt32(procentOfHworkValue) +
                                              "%) - " + pass);


                    }
                    else
                    {
                        Console.WriteLine(log.Name + " " + log.Surname + " - Brak sprawdzonej pracy domowej");
                    }

                }

            }
            else
            {
                Console.WriteLine("Brak kursantów w kursie");
            }
            Console.ReadLine();
        }
    }
}

