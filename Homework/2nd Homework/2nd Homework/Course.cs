﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer;
using BusinessLayer.Interfaces;
using DataLayer;
using DataLayer.Models;
using BusinessLayer.ModelsDto;
using BusinessLayer.Services;
using CliLayer;
using CliLayer.Menus;
using DataLayer.Repository;

namespace _2nd_Homework
{
    public class Course
    {
        private IStudentServices _studentServices = new StudentServices(new StudentRepository());
        private ICourseServices _courseServices = new CourseServices(new CourseRepository());
        public int Id;
        public void Menu()
        {
            int choose;
            choose = 0;
            while (choose != 3)
            {
                MenuCommands.CourseMenu();
             //   try
              //  {
                    choose = int.Parse(Console.ReadLine());

                    switch (choose)
                    {
                        case 1:
                            new Course().Add();
                            break;
                        case 2:
                            new CourseDetails().Menu();
                            break;
                        case 3:
                            return;
                        default:
                            Console.Clear();
                            ErrorCommands.Error();
                            break;
                    }
                    choose = new int();
               // }
              //  catch
              //  {
              //      Console.Clear();
              //      ErrorCommands.Error();
              //  }
            }
        }

        public void Add()
        {
            var course = new CourseDto();
            Console.WriteLine("Nazwa kursu?");
            course.Name = Console.ReadLine();

            var courseFromDb = _courseServices.GetCourseByName(course.Name);

            if (courseFromDb == null ||courseFromDb.Name != course.Name)
            {
                
                course.Leader = InputHelpers.InputHelper.InputHelpers.GetString("Prowadzący?");
                course.Start = InputHelpers.InputHelper.InputHelpers.GetDateTime("Data rozpoczęcia?");
                course.MinHomework = InputHelpers.InputHelper.InputHelpers.GetDouble("Mimalny % zaliczenia pracy domowej");
                if (course.MinHomework < 1 || course.MinHomework > 100)
                {
                    ErrorCommands.WrongValue();
                    return;
                }
                course.MinHomework = course.MinHomework / 100;

                course.MinPresent = InputHelpers.InputHelper.InputHelpers.GetDouble("Minimalny % obecności?");
                if (course.MinPresent < 1 || course.MinPresent > 100)
                {
                    ErrorCommands.WrongValue();
                    return;
                }
                course.MinPresent = course.MinPresent / 100;
                
                course.MaxStudent = InputHelpers.InputHelper.InputHelpers.GetInt("Maksymalna liczba kursantów?");
                if (course.MaxStudent < 1)
                {
                    ErrorCommands.TooLow();
                    return;
                }
                course.Students= new List<StudentDto>();
                
                _courseServices.AddCourse(course);

                Console.WriteLine("Dodaj kursantów");


                var student = _studentServices.TakeStudentsList();
                {
                    foreach (var log in student)
                    {

                        Console.WriteLine(log.Id + " " + log.Name + " " + log.Surname);
                        Console.WriteLine("1. Dodaj");
                        Console.WriteLine("2. Pomiń");
                        int value = InputHelpers.InputHelper.InputHelpers.GetInt("");
                        if (value == 1)
                        {
                            course.Students.Add(log);
                            
                            _courseServices.UpdateCourse(course);
                        }
                    }
                }
            }
        }

        
    }
}
