﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer;
using BusinessLayer.Interfaces;
using BusinessLayer.ModelsDto;
using BusinessLayer.Services;
using CliLayer;
using CliLayer.Menus;
using DataLayer.Repository;
using _2nd_Homework.InputHelpers;


namespace _2nd_Homework
{
    public class CourseDetails
    {
        ICourseServices service = new CourseServices(new CourseRepository());


        public void Menu()
        {



            var Id = new CourseDetails().Select();
            int choose;
            choose = 0;
            while (choose != 5)
            {
                MenuCommands.CourseDetails();

                choose = int.Parse(Console.ReadLine());


                switch (choose)
                {
                    case 1:
                        new CourseDetails().Edit(Id);
                        break;
                    case 2:
                        new CourseDetails().PresentCheck(Id);
                        break;
                    case 3:
                        new CourseDetails().HworkCheck(Id);
                        break;
                    case 4:
                        new Raport().Show(Id);
                        break;
                    case 5:
                        return;
                    default:
                        Console.Clear();
                        ErrorCommands.Error();
                        break;
                }
                choose = new int();

            }
        }

        public int Select()
        {
            var course = service.TakeCoursesList();
            Console.WriteLine("Wybierz ID kursu z listy");

            foreach (var log in course)
            {
                Console.WriteLine(log.Id + " " + log.Name);
            }

            var Id = int.Parse(Console.ReadLine());

            return Id;
        }

        public void Edit(int Id)
        {
            var course = service.TakeCourseById(Id);

            Console.WriteLine("Jeśli chcesz zmienić 'Nazwa' wpisz 'TAK'");
            var a = Console.ReadLine();
            if (a == "TAK")
            {
                var newName = InputHelpers.InputHelper.InputHelpers.GetString("Nazwa: " + course.Name);
                if (newName != "")
                {
                    course.Name = newName;
                }
            }

            Console.WriteLine("Jeśli chcesz zmienić 'Prowadzący' wpisz 'TAK'");
            var b = Console.ReadLine();
            if (b == "TAK")
            {
                var newLeader = InputHelper.InputHelpers.GetString("Prowadzący: " + course.Leader);
                if (newLeader != "")
                {
                    course.Leader = newLeader;
                }
            }
            Console.WriteLine();
            Console.WriteLine("Jeśli chcesz zmienić 'Minimalny % obecności' wpisz 'TAK'");
            var c = Console.ReadLine();
            if (c == "TAK")
            {
                var newMinPresent =
                    InputHelper.InputHelpers.GetDouble("Minimalny % obecności: " + course.MinPresent * 100);
                if (newMinPresent != course.MinHomework)

                    course.MinPresent = newMinPresent / 100;
            }
            Console.WriteLine();
            Console.WriteLine("Jeśli chcesz zmienić 'Minimalny % pracy domowej' wpisz 'TAK'");
            var d = Console.ReadLine();
            if (d == "TAK")
            {

                Console.WriteLine();
                var newMinHwork =
                    InputHelper.InputHelpers.GetDouble("Minimalny % pracy domowej" + course.MinHomework * 100);

                course.MinHomework = newMinHwork / 100;
            }
            Console.WriteLine();
            Console.WriteLine("Jeśli chcesz zmienić 'Maksymalna ilośc kursantów' wpisz 'TAK'");
            var e = Console.ReadLine();
            if (e == "TAK")
            {
                Console.WriteLine();
                var newMaxStudents =
                    InputHelper.InputHelpers.GetInt("Maksymalna ilośc kursantów: " + course.MaxStudent);

                course.MaxStudent = newMaxStudents;
            }
            List<StudentDto> toRemove = new List<StudentDto>();
            foreach (var log2 in course.Students)
            {
                Console.WriteLine("Jeśli chcesz usunąc studenta z kursu wpisz 'TAK'");
                Console.WriteLine(log2.Name + " " + log2.Surname + " " + log2.Pesel);
                var choose = (Console.ReadLine());
                if (choose == "TAK")
                {
                   
                    toRemove.Add(log2);
                }

            }
            foreach (StudentDto item in toRemove)
            {
                course.Students.Remove(item);
            }
            service.UpdateCourse(course);
        }
        



        public void PresentCheck(int Id)
        {
            var course = service.TakeCourseById(Id);
            Console.Clear();

            Questions.DateAsk();
            var _day = DateTime.Parse(Console.ReadLine());

            var date = new DateDto();


            foreach (var log in course.Students)
            {
                Console.Clear();
                Console.WriteLine("Obecnośc kursanta: ");
                Console.WriteLine(log.Name + " " + log.Surname + " " + log.Pesel);
                Console.WriteLine("w dniu " + _day + " na kursie nr: " + Id);
                Console.WriteLine();
                Console.WriteLine("1. Obecny");
                Console.WriteLine("0. Nieobecny");

                date.Present = InputHelper.InputHelpers.GetInt("");
                date.Day = _day;
                log.Courses = null;
                
                date.Student = log;
                
                course.Dates.Add(date);


                
                service.UpdateCourse(course);
            }
        }

        public void HworkCheck(int Id)
        {
            var course = service.TakeCourseById(Id);

            Console.Clear();

            Questions.DateAsk();

            var hwork = new HomeworkDto();

            Console.Clear();
            int MaxPoints = InputHelper.InputHelpers.GetInt("Maksymalna ilośc punktów?");
            if (MaxPoints < 1)
            {
                ErrorCommands.TooLow();
                return;
            }


            foreach (var log in course.Students)
            {
                Console.WriteLine(log.Name + " " + log.Surname);
                hwork.Student = log;

                hwork.MaxPoints = MaxPoints;
                
                hwork.Points = InputHelper.InputHelpers.GetInt("Ilośc punktów z pracy domowej?");
                if (hwork.Points < 1 || hwork.Points > hwork.MaxPoints)
                {
                    ErrorCommands.WrongValue();
                    return;
                }

                course.Homeworks.Add(hwork);


                
                service.UpdateCourse(course);
            }
        }
        // do tego miejsca
    }
}






