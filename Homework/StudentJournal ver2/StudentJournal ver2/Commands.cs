﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentJournal_ver2
{
    public class Commands
    {
        public void Menu()
        {
            Console.WriteLine();
            Console.WriteLine("Wybierz co chcesz zrobić: \n" +
                              "1. Rozpocznij kurs. \n" +
                              "2. Dodaj kursanta. \n" +
                              "3. Sprawdź obecność. \n" +
                              "4. Sprawdź pracę domową. \n" +
                              "5. Wyświetl raport. \n" +
                              "6. koniec.");
            Console.WriteLine();
        }

        public void Error()
        {
            Console.Clear();
            Console.WriteLine("Błędne dane.");
            Console.WriteLine("Wpisz ponownie.");
            Console.WriteLine();
        
        }

        public void CourseExist()
        {
            Console.Clear();
            Console.WriteLine("Kurs już istnieje.");
            Console.WriteLine();
        }

        public void TooLow()
        {
            Console.Clear();
            Console.WriteLine("Zbyt niska wartość.");
            Console.WriteLine("Wpisz dane ponownie.");
            
        }

        public void TooHigh()
        {
            Console.Clear();
            Console.WriteLine("Zbyt wysoka wartość.");
            Console.WriteLine("Wpisz dane ponownie.");
           
        }

        public void Success()
        {
            Console.Clear();
            Console.WriteLine("Dane zapisane poprawnie");
            Console.WriteLine();
            
        }

        public void NoStudent()
        {
            Console.Clear();
            Console.WriteLine("Dodaj napierw studenta.");
            Console.WriteLine();
            
        }

        public void DateConflict()
        {
            Console.Clear();
            Console.WriteLine("Dane dla tego dnia zostały już wprowadzone.");
            Console.WriteLine();
           
        }

        public void PresentCheck()
        {
            Console.Clear();
            Console.WriteLine("Wybierz: \n" +
                              "1 dla obecny \n" +
                              "0 dla nieobecny \n");
            Console.WriteLine();
        }

        public void DateAsk()
        {
            Console.Clear();
            Console.WriteLine("Podaj Datę:");
            Console.WriteLine();
        }

        public void CourseStart()
        {
            Console.Clear();
            Console.WriteLine("Najpierw rozpocznij kurs");
            Console.WriteLine();
        }
        
    }
}
