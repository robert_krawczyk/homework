﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace StudentJournal_ver2
{
    class Raport
    {
        public double presentValue;
        public double maxPresentValue;
        public double procentOfPresentValue;
        public double hworkValue;
        public double maxHworkValue;
        public double procentOfHworkValue;

        public void ShowRaport()
        {
            var commands = new Commands();

            if (Course.StudentList.Count == 0)
            {
                commands.NoStudent();
                return;
            }

            foreach (var log1 in Program.CourseList)
            {
                Console.WriteLine("Nazwa Kursu " + log1.Value.CourseName);
                Console.WriteLine("Data Rozpoczęcia " + log1.Value.Start.ToString("d"));
                Console.WriteLine("Minimalny procent obecności to " + log1.Value.MinPresent * 100 + "%");
                Console.WriteLine("Minimalny procent wykonania pracy domowej to " + log1.Value.MinHw * 100 + "%");
                Console.WriteLine();
                Console.WriteLine("Obecnośc");
                try
                {
                    
                    foreach (var log2 in Course.StudentList)
                    {
            

        var date = new Date();
                        presentValue = date.PresentValue(log2.Pesel);
                        maxPresentValue = date.MaxPresentValue(log2.Pesel);
                        procentOfPresentValue = presentValue / maxPresentValue * 100;
                        Convert.ToInt32(presentValue);
                        Convert.ToInt32(maxPresentValue);
                        Convert.ToInt32(procentOfPresentValue);
                        string pass = "Zaliczone";
                        if (presentValue / maxPresentValue < log1.Value.MinPresent)
                        {
                            pass = "Niezaliczone";
                        }
                        Console.WriteLine(log2.Name + " " + log2.Surname + " - "
                            + presentValue + "/" + maxPresentValue
                            + "(" + Convert.ToUInt32(procentOfPresentValue) + "%) - " + pass);
                    }
                }
                catch
                {
                    Console.WriteLine("Brak sprawdzonej obecności");
                }
                Console.WriteLine("Praca Domowa");
                try
                {
                    foreach (var log3 in Course.StudentList)
                    {
                        var hwork = new Hwork();
                        hworkValue = hwork.HworkValue(log3.Pesel);
                        maxHworkValue = hwork.MaxHworkValue(log3.Pesel);
                        procentOfHworkValue = hworkValue / maxHworkValue * 100;
                        Convert.ToInt32(hworkValue);
                        Convert.ToInt32(maxHworkValue);
                        Convert.ToInt32(procentOfHworkValue);
                        string pass = "Zaliczone";
                        if (hworkValue / maxHworkValue < log1.Value.MinHw)
                        {
                            pass = "Niezaliczone";
                        }
                        Console.WriteLine(log3.Name + " " + log3.Surname + " - " + hworkValue + "/" + maxHworkValue 
                            + "(" + Convert.ToUInt32(procentOfHworkValue) +
                                          "%) - " + pass);
                    }
                }
                catch
                {
                    Console.WriteLine("Brak sprawdzonej pracy domowej");
                }
            }
            Console.WriteLine();
        }
    }
}
    

