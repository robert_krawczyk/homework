﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace StudentJournal_ver2
{
    class Course
    {
        public string CourseName;
        public string Leader;
        public DateTime Start;
        public double MinHw;
        public double MinPresent;
        public int MaxStudents;
        
        public static List<Student> StudentList = new List<Student>();
        public static List<Date> PresentList = new List<Date>();
        public static List<Hwork> HworkList = new List<Hwork>();
        
        public void CourseAdd()
        {
            var commands = new Commands();
            if (Program.CourseList.Keys.Count == 0) 
            {
               
                try
                {
                    Course course = new Course();
                    Console.WriteLine("Nazwa kursu?");
                    course.CourseName = Console.ReadLine();
                    Console.WriteLine("Prowadzący?");
                    course.Leader = Console.ReadLine();
                    Console.WriteLine("Data rozpoczęcia?");
                    course.Start = DateTime.Parse(Console.ReadLine());
                    Console.WriteLine("Mimalny % zaliczenia pracy domowej");
                    course.MinHw = double.Parse(Console.ReadLine());
                    if (course.MinHw < 1)
                    {
                        commands.TooLow();
                        return;
                    }
                    if (course.MinHw > 100)
                    {
                        commands.TooHigh();
                        return;
                    }
                    course.MinHw = course.MinHw / 100;
                    Console.WriteLine("Minimalny % obecności?");
                    course.MinPresent = double.Parse(Console.ReadLine());
                    if (course.MinPresent < 1)
                    {
                        commands.TooLow();
                        return;
                    }
                    if (course.MinPresent > 100)
                    {
                        commands.TooHigh();
                        return;
                    }
                    course.MinPresent = course.MinPresent / 100;
                    Console.WriteLine("Liczba kursantów?");
                    course.MaxStudents = int.Parse(Console.ReadLine());
                    if (course.MaxStudents < 1)
                    {
                        commands.TooLow();
                        return;
                    }
                    Program.CourseList.Add(course.CourseName, course);
                    commands.Success();
                }
                catch
                {
                    commands.Error();
                }
            }
            else
            {
               commands.CourseExist();
            }
        }
    }
}
