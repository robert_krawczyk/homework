﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentJournal_ver2
{
    class Hwork
    {
        public long Pesel;
        public DateTime Day;
        public int Points;
        public int MaxPoints;
        
        public void HworkAdd()
        {
            var commands = new Commands();

            if (Course.StudentList.Count == 0)
            {
               commands.NoStudent();
                return;
            }

            try
            {
                commands.DateAsk();
                Day = DateTime.Parse(Console.ReadLine());
                foreach (var log in Course.HworkList)
                {
                    if (Day == log.Day)
                    {
                        commands.DateConflict();
                        return;
                    }
                }
                Console.WriteLine("Maksymalna ilośc punktów?");
                    MaxPoints = int.Parse(Console.ReadLine());
                if (MaxPoints < 0)
                {
                    commands.TooLow();
                    return;
                }

                    Console.WriteLine();
                    foreach (var log1 in Course.StudentList)
                    {
                        var hwork = new Hwork();
                        Console.WriteLine(log1.Name + " " + log1.Surname);
                        hwork.Pesel = log1.Pesel;
                        hwork.Day = Day;
                        hwork.MaxPoints = MaxPoints;
                        Console.WriteLine("Ilośc punktów z pracy domowej?");
                        hwork.Points = int.Parse(Console.ReadLine());
                        if (hwork.Points > hwork.MaxPoints)
                        {
                            commands.TooHigh();
                            return;
                        }
                        if (hwork.Points < 0)
                        {
                        commands.TooLow();
                            return;
                    }
                        Course.HworkList.Add(hwork);
                    }
                    commands.Success();
                
            }
            catch
            {
                commands.Error();
            }
        }
        public double HworkValue(long Pesel)
        {
            double hworkValue = 0;
            foreach (var log2 in Course.HworkList)
            {
                if (log2.Pesel == Pesel)
                {
                    hworkValue = hworkValue + log2.Points;
                }
            }
            return hworkValue;
        }
        public double MaxHworkValue(long Pesel)
        {
            double maxHworkValue = 0;
            foreach (var log3 in Course.HworkList)
            {
                if (log3.Pesel == Pesel)
                {
                    maxHworkValue = maxHworkValue + log3.MaxPoints;
                }

            }
            return maxHworkValue;
        }
    }
}
