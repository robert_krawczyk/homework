﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace StudentJournal_ver2
{
    class Student
    {
        public long Pesel;
        public string Name;
        public string Surname;
        public DateTime BirthDate;
        public int Sex;
        public string CourseName;
        
        public void StudentAdd()
        {
            var commands = new Commands();

            if (Program.CourseList.Count == 0)
            {
               commands.CourseStart();
                return;
            }
            try
            {
                var student = new Student();
                Console.WriteLine("Pesel");
                student.Pesel = long.Parse(Console.ReadLine());
                foreach (var log in Course.StudentList)
                {
                    if (student.Pesel == log.Pesel)
                    {
                        commands.DateConflict();
                        return;
                    }
                }
                Console.WriteLine("Imie");
                student.Name = Console.ReadLine();
                Console.WriteLine("Nazwisko");
                student.Surname = Console.ReadLine();
                Console.WriteLine("Data Urodzenia");
                student.BirthDate = DateTime.Parse(Console.ReadLine());
                Console.WriteLine("Płeć \n" +
                                  "0. Kobieta \n" +
                                  "1. Mężczyzna");
                student.Sex = int.Parse(Console.ReadLine());
                if (student.Sex > 1)
                {
                    commands.Error();
                    return;
                }
                student.CourseName = Program.CourseList.Keys.ToString();
                Course.StudentList.Add(student);
                commands.Success();
            }
            catch
            {
               commands.Error();
            }
        }
    }
}
