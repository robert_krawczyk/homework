﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;


namespace StudentJournal_ver2
{
    class Program
    {
        public static Dictionary<string,Course> CourseList = new Dictionary<string,Course>();
        
        static void Main(string[] args)
        {
            int choose;
            choose = 0;
            
            while (choose != 6)
            {
                var commands = new Commands();
                commands.Menu();

                try
                {
                    choose = int.Parse(Console.ReadLine());
                }
                catch
                { }

                switch (choose)
                {
                    case 1:
                        Console.WriteLine();
                        var course = new Course();
                        course.CourseAdd();
                        break;
                    case 2:
                        Console.WriteLine();
                        var student = new Student();
                        student.StudentAdd();
                        break;
                    case 3:
                        Console.WriteLine();
                        var date = new Date();
                        date.PresentAdd();
                        break;
                    case 4:
                        Console.WriteLine();
                        var hwork = new Hwork();
                        hwork.HworkAdd();
                        break;
                    case 5:
                        var raport = new Raport();
                        raport.ShowRaport();
                        break;
                    case 6:
                        return;

                    default:
                       
                        commands.Error();
                        break;
                }
                choose = new int();
            }
        }
    }
}
