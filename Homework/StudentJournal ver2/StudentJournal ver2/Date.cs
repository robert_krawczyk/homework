﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace StudentJournal_ver2
{
    class Date
    {
        public long Pesel;
        public DateTime Day;
        public int Present;

        public void PresentAdd()
        {
            var commands = new Commands();

            if (Course.StudentList.Count == 0)
            {
                commands.NoStudent();
                return;
            }

            try
            {
                commands.DateAsk();
                Day = DateTime.Parse(Console.ReadLine());
                foreach (var log4 in Course.PresentList)
                {
                    if (Day == log4.Day)
                    {
                      commands.DateConflict();
                        return;
                    }
                }
                commands.PresentCheck();
                foreach (var log1 in Course.StudentList)
                {
                    var date = new Date();
                    Console.WriteLine(log1.Name + " " + log1.Surname);
                    date.Pesel = log1.Pesel;
                    date.Day = Day;
                    date.Present = int.Parse(Console.ReadLine());
                    if (date.Present > 1)
                    {
                        commands.Error();
                        return;
                    }
                   
                    Course.PresentList.Add(date);
                }
                commands.Success();
            }
            catch
            {
                commands.Error();
            }
        }

        public double PresentValue(long Pesel)
        {
            double presentValue = 0;
            foreach (var log2 in Course.PresentList)
            {
                if (log2.Pesel == Pesel)
                {
                    presentValue = presentValue + log2.Present;
                }
            }
            return presentValue;
        } 

        public double  MaxPresentValue(long Pesel)
        {
            double maxPresentValue = 0;
            foreach (var log3 in Course.PresentList)
            {
                if (log3.Pesel == Pesel)
                {
                    maxPresentValue++;
                }

            }
            return maxPresentValue;
        }
    }
}
